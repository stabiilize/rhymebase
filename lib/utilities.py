import simplejson
import urllib
import json
import unicodedata
from django.http import HttpResponse

def json_response(func):
	"""
	A decorator thats takes a view response and turns it
	into json. If a callback is added through GET or POST
	the response is JSONP.
	"""	
	def decorator(request, *args, **kwargs):
		objects = func(request, *args, **kwargs)
		if isinstance(objects, HttpResponse):
			return objects
		try:
			data = simplejson.dumps(objects)
			if 'callback' in request.REQUEST:
				# a jsonp response!
				data = '%s(%s);' % (request.REQUEST['callback'], data)
				return HttpResponse(data, "text/javascript")
		except:
			data = simplejson.dumps(str(objects))
		return HttpResponse(data, "application/json")
	return decorator

def parse_json_feed(url):
	response = urllib.urlopen(url)
	return json.loads(response.read())

def parse_unicode(string):
	return unicodedata.normalize('NFKD', string).encode('ascii','ignore')

def byteify(input):
    if isinstance(input, dict):
        return {byteify(key): byteify(value) for key, value in input.iteritems()}
    elif isinstance(input, list):
        return [byteify(element) for element in input]
    elif isinstance(input, unicode):
        return input.encode('utf-8')
    else:
        return input

def date_handler(obj):
    return obj.isoformat() if hasattr(obj, 'isoformat') else obj
