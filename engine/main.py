import os
import sys

import urllib2
import xmltodict
import json
import random

sys.path.append("C:\\Users\\Mike\\projects\\mjjavantgarde\\mjavantgarde")
os.environ["DJANGO_SETTINGS_MODULE"] = "mjavantgarde.settings"

from engine.models import Video

sys.setrecursionlimit(5999)

# NOTICE
# API_KEY USES "mikejohnsonjr" GOOGLE APP, __NOT__ "rhymebase" GOOGLE APP 
# also in engine.models, engine.forms, and engine.views

API_KEY = "AIzaSyDntKKATNzobtgBK9xBeFgBkiE0h5BzbEI"

special_video = 'VuNIsY6JdUw'
song2 = 'GkD20ajVxnY'
song3 = 'Jb2stN7kH28'
song4 = 'D1Xr-JFLxik'
song5 = 'hLQl3WQQoQ0'
song6 = 'rXwMrBb2x1Q'
song7 = 'JkK8g6FMEXE'
song8 = 'fJ9rUzIMcZQ'
song9 = 'KkGVmN68ByU'
song10 = 'HU_4pf8BSQw'
song11 = 'bQ3zy9uBk_0'
song12 = '2EATQP3_fFg'
song13 = '8xg3vE8Ie_E'
song14 = 'GBVotNefYME'
songA = 'ye5BuYf8q4o'

def create_video_objects(video_id):
	next_id_set = False
	url = 'https://www.googleapis.com/youtube/v3/search?part=snippet&maxResults=50&relatedToVideoId=%s&type=video&key=%s' % (video_id, API_KEY)
	response = urllib2.urlopen(url)
	data = json.load(response)
	related_videos = data['items']
	random_int = random.randint(0,(len(related_videos)-1))

	for i,item in enumerate(related_videos):

		if (i == random_int) and not next_id_set:
			next_id = item['id']['videoId']
			next_id_set = True
			print 'NEW BASE VIDEO ID: %s' % (next_id)
		try:
			title = item["snippet"]["title"]
			video_id = item["id"]["videoId"]

			vquery_url = "https://www.googleapis.com/youtube/v3/videos?id=%s&part=contentDetails&key=%s" % (video_id,API_KEY)
			response = urllib2.urlopen(vquery_url)
			data = json.load(response) 
			caption = data["items"][0][u"contentDetails"][u"caption"]
			duration = data["items"][0][u"contentDetails"][u"duration"]


			if caption == "true":
				try:
					print "%s (%s) has captions, saving video" % (title, video_id)
					Video.objects.create(
						title=title,
						video_id=video_id,
						duration=duration)
				except Exception,e:
					print 'error: %s' % (str(e))
			else:
				pass
				#print "%s (%s) has no captions, skipping" % (title, video_id)
		except:
			pass
	create_video_objects(next_id)

create_video_objects(song14)