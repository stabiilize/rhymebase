import os
import sys
import time 

import urllib2
import json

import pafy
from pytube import YouTube

from moviepy.editor import *

from django.conf import settings

sys.path.append("C:\\Users\\Mike\\projects\\mjjavantgarde\\mjavantgarde")
os.environ["DJANGO_SETTINGS_MODULE"] = "mjavantgarde.settings"

from engine.models import Video
#from engine.models import AssortedRhymes
from engine.models import Rhyme 
from engine.models import MJJAvantGarde
from engine.models import CreatedVideo

import django
django.setup()


video_id_list = []
#ar_list = AssortedRhymes.objects.all()

mjj_list = []
rhyme_list =[]

#already_created_videos = CreatedVideo.objects.all()
#rhymes_already_used = []

#for cv in already_created_videos:
#	for r in cv.rhymes_used.all():
#		rhymes_already_used.append(r)

for m in MJJAvantGarde.objects.all():
	diff_rhymes = []
	for r in m.rhymes.all():
		if not any(d.last_word == r.last_word for d in diff_rhymes):
			if not any(d.video_id == r.video_id for d in diff_rhymes):
			#if r not in rhymes_already_used:
				diff_rhymes.append(r)

	if len(diff_rhymes) > 10 and len(diff_rhymes) < 15 and m.first_rhyme.last_word != 'ifont':
		#mjj_list.append(m)
		rhyme_list.append(diff_rhymes)
		print len(diff_rhymes)

#for mjj in mjj_list:
for _r in rhyme_list:
	#_rhymes = list(mjj.rhymes.all())
	#_rhymes.append(mjj.first_rhyme)
	for _ in _r:
		if _.video_id not in video_id_list:
			video_id_list.append(_.video_id)

print video_id_list
video_clips = []
"""
for ar in ar_list:
	if not ar.has_structure_fallback:
		video_structure = ar.structure
	else:
		video_structure = json.loads(ar.structure_fallback)
	
	for rhyme in video_structure:
		if rhyme[u'video_id'] not in video_id_list:
			video_id_list.append(rhyme[u'video_id'])
	print video_id_list
"""
#get videos
#save paths
print 'getting videos'


for video_id in video_id_list:
	print video_id
	if not os.path.isfile('%s\\media\\%s.mp4' % (settings.BASE_DIR,video_id)):
		print 'getting %s' % (video_id)
		#pytube retrival

		url = "http://www.youtube.com/watch?v=%s" % (video_id)
		#yt = YouTube(url)
		#yt.set_filename(video_id)
		#video = yt.get('mp4')
		#video.download(settings.BASE_DIR+'\\media\\')	

		#pafy retrieval
		video = pafy.new(url)
		best = video.getbest(preftype='mp4')
		best.download(
			quiet=False,
			filepath=settings.BASE_DIR+'\\media\\'+video_id+'.mp4'
		)
		video_obj = Video.objects.get(video_id=video_id)
		video_obj.video_path = settings.BASE_DIR+'\\media\\'+video_id+'.mp4'
		video_obj.save()

	else:
		print 'video %s already downloaded' % (video_id)

	#add video paths to video object video paths if not added
	#(for when errors occur during retrieval of video)
	video_obj = Video.objects.get(video_id=video_id)
	if not video_obj.video_path:
		video_obj.video_path = settings.BASE_DIR+'\\media\\'+video_id+'.mp4'
		video_obj.save()


created_video = CreatedVideo.objects.create()

def all_same(items):
    return all(x.video_id == items[0].video_id for x in items)

#dont = ['tjSnrDikc4M','US1-R-R9lYQ']
#for j,m in enumerate(rhyme_list):
for j,r in enumerate(rhyme_list):
	#rhymes = list(m.rhymes.all())
	#rhymes.append(m.first_rhyme)
	#skipped_clips = []
	
	def make_clips(video_obj_list):
		for i,s in enumerate(video_obj_list):
			

			#if s.video_id in dont:
				#continue
			#get rid of two consecutive same videos
			#if video_obj_list[i].video_id == video_obj_list[i-1].video_id:
				#dont add video obj to skipped_clips if it's already in there
				#if not any(d.full_line == s.full_line for d in skipped_clips):
					#skipped_clips.append(s)
				#delete duplicates in skipped clips
				#skipped_clips = [dict(t) for t in set([tuple(d.items()) for d in skipped_clips])]
				#continue
			created_video.rhymes_used.add(s)
			vo = Video.objects.get(video_id=s.video_id)
			start_time = float(s.start)
			end_time = float(s.start)+float(s.duration)
			print vo.video_id
			print vo.video_path
			print 'making clip for %s,start:%s,duration:%s' % (s.video_id,s.start,s.duration)
			try:
				clip = VideoFileClip(vo.video_path).subclip(
					start_time,
					end_time
				)
				txt_clip = TextClip(s.full_line,color='white',fontsize=24)
				txt_clip = txt_clip.set_pos('center').set_duration(float(s.duration))
				cclip = CompositeVideoClip([clip,txt_clip],size=(600,400))
				#print 'made clip for %s,start:%s,duration:%s,%s,%s' % (s.video_id,s.start,s.duration,is_skipped_clips,len(skipped_clips))
				print 'made clip for %s,start:%s,duration:%s' % (s.video_id,s.start,s.duration)
				#print 'clip %s of %s, set %s of %s' % (i,len(video_obj_list),j,len(mjj_list))
				print 'clip %s of %s, set %s of %s' % (i,len(video_obj_list),j,len(rhyme_list))
				video_clips.append(cclip)
			except:
				pass
			
			
			
			#if is_skipped_clips == True:
				#print skipped_clips
				#remove skipped clip

				#skipped_clips.remove(s)
				#print 'removed skipped clip %s,%s' % (s.video_id,s.start)
				#print len(skipped_clips)

		#if skipped_clips != [] and not all_same(skipped_clips):
		#	make_clips(skipped_clips,True)
	make_clips(r)

"""

video_clips = []
non_rhyming_skipped_clips = []

def all_same(items):
    return all(x[u'video_id'] == items[0][u'video_id'] for x in items)


for mjjavantgarde in AssortedRhymes.objects.all():
	skipped_clips = []
	if not mjjavantgarde.has_structure_fallback:
		structure = mjjavantgarde.structure
	else:
		structure = json.loads(mjjavantgarde.structure_fallback)

	def make_clips(video_obj_list,is_skipped_clips=False):
		for i,s in enumerate(video_obj_list):
			#get rid of two consecutive same videos
			if video_obj_list[i][u'video_id'] == video_obj_list[i-1][u'video_id']:
				#dont add video obj to skipped_clips if it's already in there
				if not any(d[u'full_line'] == s[u'full_line'] for d in skipped_clips):
					url = 'https://api.datamuse.com/words?rel_rhy=%s' % (s[u'last_word'])
					response = urllib2.urlopen(url)
					data = json.load(response)
					if any(d[u'word'] == s[u'last_word'] for d in data):
						#only append word to skipped clips if it rhymes
						skipped_clips.append(s)
					else:
						non_rhyming_skipped_clips.append(s)
				#delete duplicates in skipped clips
				#skipped_clips = [dict(t) for t in set([tuple(d.items()) for d in skipped_clips])]
				continue
			vo = Video.objects.get(video_id=s[u'video_id'])
			start_time = float(s[u'start'])
			end_time = float(s[u'start'])+float(s[u'duration'])
			print vo.video_id
			print vo.video_path
			clip = VideoFileClip(vo.video_path).subclip(
				start_time,
				end_time
			)
			print 'made clip for %s,%s,%s,%s' % (s[u'video_id'],s[u'start'],is_skipped_clips,len(skipped_clips))
			
			
			video_clips.append(clip)
			if is_skipped_clips == True:
				print skipped_clips
				#remove skipped clip

				skipped_clips.remove(s)
				print 'removed skipped clip %s,%s' % (s[u'video_id'],s[u'start'])
				print len(skipped_clips)
				print len(non_rhyming_skipped_clips)

		if skipped_clips != [] and not all_same(skipped_clips):
			make_clips(skipped_clips,True)



	make_clips(structure)

"""
time.sleep(120)
final = concatenate_videoclips(video_clips,method='compose')
final.write_videofile("mjjavantgarde-random-mix4.mp4",fps=24)