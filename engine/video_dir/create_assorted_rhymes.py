import os
import sys
import json
import urllib2

sys.path.append("C:\\Users\\Mike\\projects\\mjjavantgarde\\mjavantgarde")
os.environ["DJANGO_SETTINGS_MODULE"] = "mjavantgarde.settings"

from engine.models import Video
from engine.models import Rhyme
from engine.models import AssortedRhymes

import django
django.setup()

rhyme_list = Rhyme.objects.filter(is_assorted=False)

#first_rhyme_list = []

#for r in rhyme_list:
#	if not r.parent:
#		first_rhyme_list.append(r)

for r in rhyme_list:
	print 'working with %s' % (r.last_word)
	url = 'https://api.datamuse.com/words?rel_rhy=%s' % (r.last_word)
	response = urllib2.urlopen(url)
	data = json.load(response)

	rhyme_word_list = [d[u'word'] for d in data]
	rhyme_word_list.append(r.last_word)
	#first word
	found_match = False
	for word in rhyme_word_list:
		if AssortedRhymes.objects.filter(first_rhyme__last_word=word.lower()).exists():
			found_match = True
			match = word
			break
		else:
			continue

	if found_match:
		try:
			m = AssortedRhymes.objects.get(first_rhyme__last_word=match.lower())
			m.rhymes.add(r)
			print 'added %s to AssortedRhymes(first_rhyme=%s)' % (r.last_word,word)
		except Exception,e:
			print 'error: %s' % (str(e))
			# add duplicate assortedrhymes to largest and delete dupes
			ms = AssortedRhymes.objects.filter(first_rhyme__last_word=match.lower())
			arl = [len(m.rhymes.all()) for m in ms]
			largest_m_index = arl.index(max(arl))
			largest_m = ms[largest_m_index]
			msl = list(ms)
			msl.pop(largest_m_index)
			for m in msl:
				largest_m.rhymes.add(m.first_rhyme)
				m.delete()
			largest_m.rhymes.add(r)
			print 'dupes added to largest and deleted'

	else:
		AssortedRhymes.objects.create(first_rhyme=r)
		print 'created AssortedRhymes(first_rhyme=%s)' % (r.last_word)

	r.is_assorted=True
	r.save()

	"""
	#rest of words
	for r in fr.rhyme_set.all():
		print 'working with %s' % (r.last_word)
		_url = 'https://api.datamuse.com/words?rel_rhy=%s' % (r.last_word)
		_response = urllib2.urlopen(_url)
		_data = json.load(_response)

		_first_rhyme_word_list = [d[u'word'] for d in data]
		_first_rhyme_word_list.append(r.last_word)

		_found_match = False
		for _word in first_rhyme_word_list:
			if MJJAvantGarde.objects.filter(first_rhyme__last_word=_word.lower()).exists():
				_found_match = True
				_match = _word
				break
			else:
				continue
		if _found_match:
			m = MJJAvantGarde.objects.get(first_rhyme__last_word=_match.lower())
			m.rhymes.add(r)
			print 'added %s to MJJAvantGarde(first_rhyme=%s)' % (r.last_word,_match)
		else:
			MJJAvantGarde.objects.create(first_rhyme=r)
			print 'created MJJAvantGarde(first_rhyme=%s)' % (r.last_word)

		r.is_assorted = True
		r.save()
	"""

"""
compv_list = ComposedVideo.objects.all()
rhyme_list = []


for i,v in enumerate(compv_list):
	
	if not v.has_structure_fallback:
		structure = v.structure
	else:
		structure = json.loads(v.structure_fallback)

	#get first rhyme word
	first_rhyme = structure[0][u'last_word']

	#get words that rhyme with first rhyme
	url = 'https://api.datamuse.com/words?rel_rhy=%s' % (first_rhyme)
	response = urllib2.urlopen(url)
	data = json.load(response)
	first_rhyme_word_list = [d[u'word'] for d in data]
	first_rhyme_word_list.append(first_rhyme)

	#check to see if there is an assortedrhymes object that already rhymes 
	#with the first rhyme word
	print 'working with first word in composedvideo %s' % (i) 
	found_match = False
	for word in first_rhyme_word_list:
		#print 'word: %s, first_rhyme: %s' % (word,first_rhyme)
		if AssortedRhymes.objects.filter(rhyme_word=word).exists():
			found_match = True
			match = word
		else:
			continue

	if found_match:
		print 'found match %s for first word in composedvideo %s' % (match,i)
		ar = AssortedRhymes.objects.get(rhyme_word=match)
		if not ar.has_structure_fallback:
			print 'assortedrhymes(rhyme_word=%s) has regular structure' % (match)
			print 'structure 0 full line: %s' % (structure[0][u'full_line'])
			print 'ar.structure: %s' % (ar.structure)
			if not any(d[u'full_line'] == structure[0][u'full_line'] for d in ar.structure):
				print 'assortedrhymes object for %s exists, appending' % (first_rhyme)
				ar.structure.append(structure[0])
				ar.save()
			else:
				print structure[0]
				print 'line %s already exists in assortedrhymes(rhyme_word=%s)' % (structure[0][u'full_line'],match)
		else:
			print 'assortedrhymes(rhyme_word=%s) has fallback structure' % (match)
			if not any(d[u'full_line'] == structure[0][u'full_line'] for d in ar.structure_fallback):
				arstructure = json.loads(ar.structure_fallback[0])
				arstructure.append(structure[0])
				ar.structure_fallback = arstructure
				ar.save()
			else:
				print 'line %s already exists in assortedrhymes(rhyme_word=%s)' % (structure[0][u'full_line'],match)
	else:
		print 'assortedrhymes object for %s does not exist, creating new object' % (first_rhyme)
		print structure[0]
		try:
			ar = AssortedRhymes.objects.create(
				rhyme_word=first_rhyme,
				structure=[structure[0]],
			)
		except:
			ar = AssortedRhymes.objects.create(
				rhyme_word=first_rhyme,
				has_structure_fallback=True,
				structure_fallback=json.dumps([structure[0]])
			)
	print 'working with rest of words'
	for s in structure:
		lwfound_match = False
		last_word = s[u'last_word']
		print 'last word: %s' % last_word
		lwurl = 'https://api.datamuse.com/words?rel_rhy=%s' % (last_word)
		lwresponse = urllib2.urlopen(lwurl)
		lwdata = json.load(lwresponse)
		last_word_list = [d[u'word'] for d in lwdata]
		last_word_list.append(last_word)
		for word in last_word_list:
			#print 'word: %s, last_word: %s' % (word,last_word)
			if AssortedRhymes.objects.filter(rhyme_word=word).exists():
				lwfound_match = True
				lwmatch = word
			else: 
				continue
		if lwfound_match:
			print 'found match %s for first word in composedvideo %s' % (lwmatch,i)
			ar = AssortedRhymes.objects.get(rhyme_word=lwmatch)
			if not ar.has_structure_fallback:
				print 'assortedrhymes(rhyme_word=%s) has regular structure' % (lwmatch)
				print 's_full_line: %s' % (s[u'full_line'])
				print 'ar.structure: %s' % (ar.structure)
				if not any(d[u'full_line'] == s[u'full_line'] for d in ar.structure):
					print 'assortedrhymes object for %s exists, appending to assortedrhymes(rhyme_word=%s)' % (last_word,lwmatch)
					ar.structure.append(s)
					ar.save()
				else:
					print ar.structure
					print 'line %s already exists in assortedrhymes(rhyme_word=%s)' % (s[u'full_line'],lwmatch)
			else:
				print 'assortedrhymes(rhyme_word=%s) has fallback structure' % (lwmatch)
				print 's_full_line: %s' % (s[u'full_line'])
				print 'ar.structure_fallback: %s' % (json.loads(ar.structure_fallback))
				if not any(d[u'full_line'] == s[u'full_line'] for d in json.loads(ar.structure_fallback)):
					arstructure = json.loads(ar.structure_fallback)
					arstructure.append(s)
					ar.structure_fallback = arstructure
					ar.save()
				else:
					print 'line %s already exists in assortedrhymes(rhyme_word=%s)' % (s[u'full_line'],lwmatch)
		else:
			print 'assortedrhymes object for %s does not exist, creating new object' % (last_word)
			try:
				ar = AssortedRhymes.objects.create(
					rhyme_word=last_word,
					structure=[s],
				)
			except:
				ar = AssortedRhymes.objects.create(
					rhyme_word=last_word,
					has_structure_fallback=True,
					structure_fallback=json.dumps([s])
				)
"""