import os
import sys
import re
import urllib2
import json



sys.path.append("C:\\Users\\Mike\\projects\\mjjavantgarde\\mjavantgarde")
os.environ["DJANGO_SETTINGS_MODULE"] = "mjavantgarde.settings"

from engine.models import Video
from engine.models import Rhyme

import django
django.setup()

remove = [u'\u266a',u'\n']
video_list = Video.objects.filter(has_rhymes=False,has_captions=True)

for v in video_list:

	print 'getting rhymes for %s' % (v.title)
	for caption in v.captions_json[u'transcript'][u'text']:
		if not u'#text' in caption:
			continue
		try:	
			caption_text = caption[u'#text']
		except:
			continue
		for substr in remove:
			caption_text = caption_text.replace(substr,'')
		caption_text = re.sub('[^a-zA-Z0-9 \n\.]', '', caption_text)
		caption_text = caption_text.strip()
		words = caption_text.split(' ')

		#get last word
		last_word = words[-1]

		caption[u'last_word'] = re.sub('[^a-zA-Z0-9]', '', last_word).lower()

		#get rhymes
		#url = 'https://api.datamuse.com/words?rel_rhy=%s' % (last_word)
		#response = urllib2.urlopen(url)
		#data = json.load(response)
		#caption[u'last_word_rhymes'] = data
		try:
			rhyme = Rhyme.objects.create(
				video_id=v.video_id,
				last_word=caption[u'last_word'],
				start=caption[u'@start'],
				duration=caption[u'@dur'],
				video_title=v.title,
				full_line=caption_text,
				full_line_raw=caption[u'#text']
			)
			#print 'rhyme saved for caption %s, added to %s' % (caption_text,v.video_id)
		except Exception,e:
			print 'error: %s' % (str(e))

	v.has_rhymes = True
	v.save()