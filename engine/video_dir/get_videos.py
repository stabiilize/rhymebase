import os
import sys

import pafy

sys.path.append("C:\\Users\\Mike\\projects\\mjjavantgarde\\mjavantgarde")
os.environ["DJANGO_SETTINGS_MODULE"] = "mjavantgarde.settings"

from engine.models import Video

video_list = Video.objects.filter(has_captions=True)

for v in video_list:
	v = pafy.new(v.video_id)
	s = v.getbest()
	print 'downloading %s' % v.title
	print("Size is %s" % s.get_filesize())
	filename = s.download()  # starts download