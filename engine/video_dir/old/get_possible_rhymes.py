import os
import sys

sys.path.append("C:\\Users\\Mike\\projects\\mjjavantgarde\\mjavantgarde")
os.environ["DJANGO_SETTINGS_MODULE"] = "mjavantgarde.settings"

from engine.models import Video

video_list = Video.objects.filter(has_rhymes=True,has_possible_rhymes=False)

for i,v in enumerate(video_list):

	print 'getting possible rhymes for %s:%s' % (i,v.title)
	for caption in v.captions_json[u'transcript'][u'text']:
		if not u'#text' in caption:
			caption[u'possible_rhymes'] = []
			v.save()
			continue
		caption_text = caption[u'#text']
		last_word = caption[u'last_word']
		last_word_rhymes = caption[u'last_word_rhymes']
		caption[u'possible_rhymes'] = []
		v.save()
		print 'last_word: %s, possible_rhymes: %s' % (last_word,caption[u'possible_rhymes'])
		#store possible rhymes 
		for l in last_word_rhymes:
			print 'word: %s' % (l[u'word'])
			for prv in Video.objects.filter(has_rhymes=True):
				#print 'video: %s' % (prv.video_id)
				for prv_caption in prv.captions_json[u'transcript'][u'text']:
					if not u'last_word' in prv_caption:
						continue
					#print 'caption last word: %s' % (prv_caption[u'last_word'])
					try:
						if l[u'word'] == prv_caption[u'last_word']:
							print 'match found!'
							if not any(d['full_line'] == caption[u'#text'] for d in caption[u'possible_rhymes']):
								caption[u'possible_rhymes'].append({
									'full_line': caption[u'#text'],
									'video_id': v.video_id,
									'video_title': v.title,
									'start': caption[u'@start'],
									'duration': caption[u'@dur'],
									'last_word': caption[u'last_word'],
								})
								v.save()
								print caption[u'possible_rhymes']
							if not any(d['full_line'] == prv_caption[u'#text'] for d in caption[u'possible_rhymes']):
								caption[u'possible_rhymes'].append({
									'full_line': prv_caption[u'#text'],
									'video_id': prv.video_id,
									'video_title': prv.title,
									'start': prv_caption[u'@start'],
									'duration': prv_caption[u'@dur'],
									'last_word': prv_caption[u'last_word'],
								})
								v.save()
								print caption[u'possible_rhymes']
								print 'saved possible rhyme %s in %s for %s:%s' % (
									prv_caption[u'last_word'],
									prv.title,
									v.title,
									last_word)
					except KeyError:
						pass
						#print 'failed rhyme retrieval for %s:%s' % (v.title,last_word)
	v.has_possible_rhymes = True
	v.save()