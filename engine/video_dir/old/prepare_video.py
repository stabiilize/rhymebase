import os
import sys
import json

import zlib
import base64

sys.path.append("C:\\Users\\Mike\\projects\\mjjavantgarde\\mjavantgarde")
os.environ["DJANGO_SETTINGS_MODULE"] = "mjavantgarde.settings"

from engine.models import Video
#from engine.models import ComposedVideo
from engine.models import Rhyme

import django
django.setup()
#edit

mjj_title = 'mjjavantgarde random mix'
mjj_genre = 'random'

video_list = Video.objects.filter(has_possible_rhymes=True)

for v in video_list:
	possible_rhymes = []
	for c in v.captions_json[u'transcript'][u'text']:
		first_rhyme = None
		if not c[u'possible_rhymes'] == []:
			for i,cc in enumerate(c[u'possible_rhymes']):
				try:
					if i != 0:
						Rhyme.objects.create(
							parent=first_rhyme,
							video_id=cc[u'video_id'],
							last_word=cc[u'last_word'].lower(),
							start=cc[u'start'],
							duration=cc[u'duration'],
							video_title=cc[u'video_title'],
							full_line=cc[u'full_line']
						)
					else:

						first_rhyme = Rhyme.objects.create(
							video_id=cc[u'video_id'],
							last_word=cc[u'last_word'].lower(),
							start=cc[u'start'],
							duration=cc[u'duration'],
							video_title=cc[u'video_title'],
							full_line=cc[u'full_line']
						)
				except Exception,e:
					print 'error: %s' % (str(e))
"""
	if possible_rhymes != []:
		try:
			compv = ComposedVideo.objects.create(
				structure=possible_rhymes,
				title=mjj_title,
				genre=mjj_genre)
			v.composed_videos.add(compv)
			v.has_composed_videos = True
			v.save()
			print 'composed video structure created'

		except Exception,e:
			print 'error: %s' % (str(e))
			try:
				print len(json.dumps(possible_rhymes))
				compv = ComposedVideo.objects.create(
					structure=None,
					has_structure_fallback=True,
					structure_fallback=json.dumps(possible_rhymes),
					title=mjj_title,
					genre=mjj_genre)
				v.composed_videos.add(compv)
				v.has_composed_videos = True
				v.save()

				print "composed video structure created with textfield fallback"
			except Exception,e:
				print 'error2: %s' % (str(e))
	else: 
		print 'possible_rhymes == []'
"""