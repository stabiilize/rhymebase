import random
import string
import urllib2
import json
import re

from django.template.defaultfilters import slugify
from django.core.urlresolvers import reverse,reverse_lazy
from django.db.models.signals import post_save
from django.db import models

#from django.contrib.postgres.fields import JSONField
from django.contrib.auth.models import User

from vote.models import VoteModel

class JSONField(models.TextField):
    """
    JSONField is a generic textfield that neatly serializes/unserializes
    JSON objects seamlessly.
    Django snippet #1478

    example:
        class Page(models.Model):
            data = JSONField(blank=True, null=True)


        page = Page.objects.get(pk=5)
        page.data = {'title': 'test', 'type': 3}
        page.save()
    """

    #__metaclass__ = models.SubfieldBase

    def from_db_value(self, value, expression, connection, context):
        return self.to_python(value)

    def to_python(self, value):
        if value == "":
            return None

        try:
            if isinstance(value, basestring):
                return json.loads(value)
        except ValueError:
            pass
        return value

    def get_db_prep_save(self, value, *args, **kwargs):
        if value == "":
            return None
        if isinstance(value, dict):
            value = json.dumps(value, cls=DjangoJSONEncoder)
        return super(JSONField, self).get_db_prep_save(value, *args, **kwargs)

# NOTICE
# API_KEY USES "mikejohnsonjr" GOOGLE APP, __NOT__ "rhymebase" GOOGLE APP 
# also in main.py, engine.forms, and engine.views

API_KEY = "AIzaSyDntKKATNzobtgBK9xBeFgBkiE0h5BzbEI"

class Counts(models.Model):
	rhymes = models.BigIntegerField()
	assorted_rhymes = models.BigIntegerField()
	genres = models.BigIntegerField()
	artists = models.BigIntegerField()
	songs = models.BigIntegerField()

#karma for adding rhymes, upvotes, 1/2 karma for upvoting
class Rhyme(VoteModel,models.Model):

	user = models.ForeignKey(User,blank=True,null=True)

	is_community = models.BooleanField(default=False)
	original_rhyme = models.ForeignKey('self',blank=True,null=True)
	community_rhymes = models.ManyToManyField('self',blank=True)

	video_id = models.CharField(max_length=20)
	last_word = models.CharField(max_length=50)
	start = models.CharField(max_length=20)
	duration = models.CharField(max_length=20)
	video_title = models.TextField()
	full_line = models.TextField()
	full_line_raw = models.TextField(blank=True)
	is_assorted = models.BooleanField(default=False)
	date_added = models.DateTimeField(auto_now_add=True,blank=True,null=True)

	slug = models.SlugField(blank=True,null=True,max_length=255)

	likely_needs_editing = models.BooleanField(default=False)

	is_added_to_video = models.BooleanField(default=False)

	def get_published_rhyme(self,*args,**kwargs):
		community_rhymes = self.community_rhymes
		if community_rhymes == []:
			return self
		else:
			votes = {r.id:r.votes_score for r in community_rhymes}
			if votes != {}:
				most_votes = max(votes,key=votes.get)
				if votes[most_votes] > 0:
					return Rhyme.objects.get(id=most_votes)
				else:
					return self
			else:
				return self

	def save(self,*args,**kwargs):

		if not self.slug:
			pr = self.get_published_rhyme()
			fl = pr.full_line
			self.slug = slugify(fl)[:255]

		if not self.last_word:
			words = self.full_line.split(' ')
			last_word = words[-1]
			self.last_word = re.sub('[^a-zA-Z0-9]', '', last_word).lower()
		
		super(Rhyme,self).save(*args,**kwargs)


	def __unicode__(self):
		return unicode(self.full_line)

	def get_absolute_url(self):
		return reverse("rhyme_detail", kwargs={
			'video_id':str(self.video_id),
			'pk':str(self.id),
			'slug':str(self.slug)})

	def get_user_voted(self,user_id):
		if self.votes.exists(user_id,action=0):
			vote = 'voted_up'
		elif self.votes.exists(user_id,action=1):
			vote = 'voted_down'
		else:
			vote = 'not_voted'
		return vote

class AssortedRhymes(models.Model):
	first_rhyme = models.ForeignKey(Rhyme)
	rhymes = models.ManyToManyField(Rhyme,related_name='rhymes')

class UserProfile(models.Model):
	user = models.OneToOneField(User)
	status = models.CharField(max_length=150,blank=True,null=True)
	reddit_username = models.CharField(max_length=50,blank=True,null=True)
	community_genres = models.ManyToManyField('CommunityGenre',blank=True)
	community_artists = models.ManyToManyField('CommunityArtist',blank=True)
	community_songs = models.ManyToManyField('CommunitySong',blank=True)
	rhymes = models.ManyToManyField(Rhyme,blank=True)

	def __unicode__(self):
		return self.user

	def get_karma(self,*args,**kwargs):
		user = self.user
		rhymes = self.rhymes.all()
		genres = self.community_genres.all()
		artists = self.community_artists.all()
		songs = self.community_songs.all()
		karma = 0

		for r in rhymes:
			karma += r.votes.count()
		for g in genres:
			karma += g.votes.count()
		for a in artists:
			karma += a.votes.count()
		for s in songs:
			karma += s.votes.count()

		karma += (len(Rhyme.objects.all()[0].votes.all(user.id))/5.0)
		if len(CommunityGenre.objects.all()) > 0:
			karma += (len(CommunityGenre.objects.all()[0].votes.all(user.id))/5.0)
		if len(CommunityArtist.objects.all()) > 0:
			karma += (len(CommunityArtist.objects.all()[0].votes.all(user.id))/5.0)
		if len(CommunitySong.objects.all()) > 0:
			karma += (len(CommunitySong.objects.all()[0].votes(user.id))/5.0)

		return format(karma,'.2f')

class ArtistManager(models.Manager):

	def get_by_natural_key(self,artist):
		return self.get(artist=artist)

class Artist(models.Model):
	artist = models.CharField(max_length=50,unique=True)
	slug = models.SlugField(max_length=255,blank=True,null=True)
	videos = models.ManyToManyField('Video',blank=True)

	objects = ArtistManager()

	def natural_key(self):
		return self.genre

	def get_picked_videos(self,*args,**kwargs):
		videos = self.videos
		community_artists = [v.get_community_artist() for v in videos]
		picked_video_ids = [ca.video_id for ca in community_artists if cg.artist_object == self]
		picked_videos = [Video.objects.get(video_id=v_id) for v_id in picked_video_ids]
		return picked_videos

	def __unicode__(self):
		return self.artist

	def save(self,*args,**kwargs):
		
		if not self.slug:
			self.slug = slugify(self.artist)

		super(Artist,self).save(*args,**kwargs)

class CommunityArtist(VoteModel,models.Model):
	user = models.ForeignKey(User)
	artist_object = models.ForeignKey(Artist)
	date_added = models.DateTimeField(auto_now_add=True)
	video_id = models.CharField(max_length=20,blank=True,null=True)
	video_title = models.CharField(max_length=255,blank=True)

	is_added_to_video = models.BooleanField(default=False)

	def get_absolute_url(self):
		return reverse("video_detail", kwargs={
			'video_id':str(self.video_id)})

	def __unicode__(self):
		return self.artist_object.artist

	def get_user_voted(self,user_id):
		return self.votes.exists(user_id)

class SongManager(models.Manager):

	def get_by_natural_key(self,song):
		return self.get(song=song)

class Song(models.Model):
	song = models.CharField(max_length=50,unique=True)
	slug = models.SlugField(max_length=255,blank=True,null=True)
	videos = models.ManyToManyField('Video',blank=True)

	objects = SongManager()

	def natural_key(self):
		return self.genre

	def get_picked_videos(self,*args,**kwargs):
		videos = self.videos
		community_songs = [v.get_community_song() for v in videos]
		picked_video_ids = [cs.video_id for cs in community_songs if cg.genre_song == self]
		picked_videos = [Video.objects.get(video_id=v_id) for v_id in picked_video_ids]
		return picked_videos

	def __unicode__(self):
		return self.song

	def save(self,*args,**kwargs):
		
		if not self.slug:
			self.slug = slugify(self.song)

		super(Song,self).save(*args,**kwargs)

	def get_user_voted(self,user_id):
		if self.votes.exists(user_id,action=0):
			vote = 'voted_up'
		elif self.votes.exists(user_id,action=1):
			vote = 'voted_down'
		else:
			vote = 'not_voted'
		return vote

class CommunitySong(VoteModel,models.Model):
	user = models.ForeignKey(User)
	song_object = models.ForeignKey(Song)
	date_added = models.DateTimeField(auto_now_add=True)
	video_id = models.CharField(max_length=20,blank=True,null=True)

	is_added_to_video = models.BooleanField(default=False)
	video_title = models.CharField(max_length=255,blank=True)


	def get_absolute_url(self):
		return reverse("video_detail", kwargs={
			'video_id':str(self.video_id)})

	def __unicode__(self):
		return self.song_object.song

	def get_user_voted(self,user_id):
		if self.votes.exists(user_id,action=0):
			vote = 'voted_up'
		elif self.votes.exists(user_id,action=1):
			vote = 'voted_down'
		else:
			vote = 'not_voted'
		return vote

class GenreManager(models.Manager):

	def get_by_natural_key(self,genre):
		return self.get(genre=genre)

class Genre(models.Model):
	genre = models.CharField(max_length=50,unique=True)
	slug = models.SlugField(max_length=255,blank=True,null=True)
	videos = models.ManyToManyField('Video',blank=True)

	objects = GenreManager()


	def natural_key(self):
		return self.genre

	def get_picked_videos(self,*args,**kwargs):
		videos = self.videos.all()
		community_genres = [v.get_community_genre() for v in videos]
		picked_video_ids = [cg.video_id for cg in community_genres if cg.genre_object == self]
		picked_videos = [Video.objects.get(video_id=v_id) for v_id in picked_video_ids]
		return picked_videos

	def __unicode__(self):
		return self.genre

	def save(self,*args,**kwargs):
		
		if not self.slug:
			self.slug = slugify(self.genre)

		super(Genre,self).save(*args,**kwargs)

class CommunityGenre(VoteModel,models.Model):
	user = models.ForeignKey(User)
	genre_object = models.ForeignKey(Genre)
	date_added = models.DateTimeField(auto_now_add=True)
	video_id = models.CharField(max_length=20,blank=True,null=True)

	is_added_to_video = models.BooleanField(default=False)

	video_title = models.CharField(max_length=255,blank=True)

	def get_absolute_url(self):
		return reverse("video_detail", kwargs={
			'video_id':str(self.video_id)})

	def __unicode__(self):
		return self.genre_object.genre

	def get_user_voted(self,user_id):
		if self.votes.exists(user_id,action=0):
			vote = 'voted_up'
		elif self.votes.exists(user_id,action=1):
			vote = 'voted_down'
		else:
			vote = 'not_voted'
		return vote

class Video(models.Model):
	title = models.CharField(max_length=255)
	video_id = models.CharField(max_length=20,unique=True)
	duration = models.CharField(max_length=10)
	captions_json = JSONField(blank=True,null=True)
	has_captions = models.BooleanField(default=False,blank=True)
	caption_retrieval_failed = models.BooleanField(default=False,blank=True)
	has_rhymes = models.BooleanField(default=False,blank=True)

	is_music_video = models.BooleanField(default=False,blank=True)

	rhymes = models.ManyToManyField(Rhyme,blank=True)
	community_genres = models.ManyToManyField(CommunityGenre,blank=True)
	community_artists = models.ManyToManyField(CommunityArtist,blank=True)
	community_songs = models.ManyToManyField(CommunitySong,blank=True)

	thumbnail = models.CharField(max_length=255,blank=True)

	def __unicode__(self):
		return unicode("%s:%s" % (self.video_id,self.title))

	def get_community_genre(self,*args,**kwargs):

		community_genres = self.community_genres.all()
		if community_genres == []:
			return None
		else:
			votes = {g.id:g.vote_score for g in community_genres}
			if votes != {}:
				most_votes = max(votes,key=votes.get)
				if votes[most_votes] > 0:
					return CommunityGenre.objects.get(id=most_votes)
				else:
					return None
			else:
				return None

	def get_community_song(self,*args,**kwargs):

		community_songs = self.community_songs.all()
		if community_songs == []:
			return None
		else:
			votes = {g.id:g.vote_score for g in community_songs}
			if votes != {}:
				most_votes = max(votes,key=votes.get)
				if votes[most_votes] > 0:
					return CommunitySong.objects.get(id=most_votes)
				else:
					return None
			else:
				return None

	def get_community_artist(self,*args,**kwargs):

		community_artists = self.community_artists.all()
		if community_artists == []:
			return None
		else:
			votes = {g.id:g.votes_score for g in community_artists}
			if votes != {}:
				most_votes = max(votes,key=votes.get)
				if votes[most_votes] > 0:
					return CommunityArtist.objects.get(id=most_votes)
				else:
					return None
			else:
				return None

def create_profile(sender, instance, created, **kwargs):
	if created: 
		_u = User.objects.get(username=instance)
		profile, created = UserProfile.objects.get_or_create(
			user=instance)

post_save.connect(create_profile, sender=User)

def add_song_to_video(sender, instance, created, **kwargs):
	if created:
		cs = instance
		if not cs.is_added_to_video:
			video_id = cs.video_id
			if not Video.objects.filter(video_id=video_id).exists():
				url = "https://www.googleapis.com/youtube/v3/videos?id=%s&part=snippet,contentDetails&key=%s" % (video_id,API_KEY)
				response = urllib2.urlopen(url)
				data = json.load(response) 
				title = data["items"][0][u"snippet"][u"title"]
				video_object = Video.objects.create(video_id=video_id,title=title)
			else:
				video_object = Video.objects.get(video_id=video_id)
			
			video_object.community_genres.add(cs)
			cs.is_added_to_video = True

		if not cs.votes.exists(cs.user.id):
			cs.votes.up(cs.user.id)
			cs.user.userprofile.community_songs.add(cs)

post_save.connect(add_song_to_video,sender=CommunitySong)

def add_artist_to_video(sender, instance, created, **kwargs):
	if created:
		ca = instance
		if not ca.is_added_to_video:
			video_id = ca.video_id
			if not Video.objects.filter(video_id=video_id).exists():
				url = "https://www.googleapis.com/youtube/v3/videos?id=%s&part=snippet,contentDetails&key=%s" % (video_id,API_KEY)
				response = urllib2.urlopen(url)
				data = json.load(response) 
				title = data["items"][0][u"snippet"][u"title"]
				video_object = Video.objects.create(video_id=video_id,title=title)
			else:
				video_object = Video.objects.get(video_id=video_id)
			
			video_object.community_genres.add(ca)
			ca.is_added_to_video = True

		if not ca.votes.exists(ca.user.id):
			ca.votes.up(cg.user.id)
			ca.user.userprofile.community_artists.add(ca)

post_save.connect(add_artist_to_video,sender=CommunityArtist)

def add_genre_to_video(sender, instance, created, **kwargs):
	if created:
		cg = instance
		if not cg.is_added_to_video:
			video_id = cg.video_id
			if not Video.objects.filter(video_id=video_id).exists():
				url = "https://www.googleapis.com/youtube/v3/videos?id=%s&part=snippet,contentDetails&key=%s" % (video_id,API_KEY)
				response = urllib2.urlopen(url)
				data = json.load(response) 
				title = data["items"][0][u"snippet"][u"title"]
				thumbnail = data["items"][0][u"snippet"][u"thumbnails"][u"standard"][u"url"]
				video_object = Video.objects.create(video_id=video_id,title=title,thumbnail=thumbnail)
			else:
				video_object = Video.objects.get(video_id=video_id)
			
			video_object.community_genres.add(cg)
			cg.is_added_to_video = True

		if not cg.votes.exists(cg.user.id):
			cg.votes.up(cg.user.id)
			cg.user.userprofile.community_genres.add(cg)

post_save.connect(add_genre_to_video,sender=CommunityGenre)

def rhyme_post_save(sender, instance, created, **kwargs):
	if created:
		rhyme = instance

		if rhyme.is_community:
			rhyme.original_rhyme.community_rhymes.add(rhyme)

		if not rhyme.votes.exists(rhyme.user.id):
			rhyme.votes.up(rhyme.user.id)

			rhyme.user.userprofile.rhymes.add(rhyme)
			rhyme.user.userprofile.save()

		if not rhyme.is_added_to_video:
			video = Video.objects.get(video_id=rhyme.video_id)
			video.rhymes.add(rhyme)
			rhyme.is_added_to_video = True
			video.save()

		#assort rhyme on save
		if not AssortedRhymes.objects.filter(rhymes=rhyme).exists():
			d_url = 'https://api.datamuse.com/words?rel_rhy=%s' % (rhyme.last_word)
			d_response = urllib2.urlopen(d_url)
			d_data = json.load(d_response)
			rhyme_word_list = [d[u'word'] for d in d_data]
			rhyme_word_list.append(rhyme.last_word)
			found_match = False
			for word in rhyme_word_list:
				if AssortedRhymes.objects.filter(first_rhyme__last_word=word.lower()).exists():
					found_match = True
					match = word
					break
				else:
					continue

			if found_match:
				m = AssortedRhymes.objects.get(first_rhyme__last_word=match.lower())
				m.rhymes.add(rhyme)
			else:
				AssortedRhymes.objects.create(first_rhyme=rhyme)

post_save.connect(rhyme_post_save,sender=Rhyme)
