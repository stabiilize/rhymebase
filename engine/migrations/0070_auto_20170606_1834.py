# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('engine', '0069_rhyme_community_rhymes'),
    ]

    operations = [
        migrations.CreateModel(
            name='Artist',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('artist', models.CharField(unique=True, max_length=50)),
                ('slug', models.SlugField(max_length=255, null=True, blank=True)),
            ],
        ),
        migrations.CreateModel(
            name='CommunityArtist',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('date_added', models.DateTimeField(auto_now_add=True)),
                ('video_id', models.CharField(max_length=20, null=True, blank=True)),
                ('is_added_to_video', models.BooleanField(default=False)),
                ('artist_object', models.ForeignKey(to='engine.Artist')),
                ('user', models.ForeignKey(to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.CreateModel(
            name='CommunitySong',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('date_added', models.DateTimeField(auto_now_add=True)),
                ('video_id', models.CharField(max_length=20, null=True, blank=True)),
                ('is_added_to_video', models.BooleanField(default=False)),
            ],
        ),
        migrations.CreateModel(
            name='Song',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('song', models.CharField(unique=True, max_length=50)),
                ('slug', models.SlugField(max_length=255, null=True, blank=True)),
            ],
        ),
        migrations.AddField(
            model_name='video',
            name='is_music_video',
            field=models.BooleanField(default=False),
        ),
        migrations.AddField(
            model_name='song',
            name='videos',
            field=models.ManyToManyField(to='engine.Video', blank=True),
        ),
        migrations.AddField(
            model_name='communitysong',
            name='song_object',
            field=models.ForeignKey(to='engine.Song'),
        ),
        migrations.AddField(
            model_name='communitysong',
            name='user',
            field=models.ForeignKey(to=settings.AUTH_USER_MODEL),
        ),
        migrations.AddField(
            model_name='artist',
            name='videos',
            field=models.ManyToManyField(to='engine.Video', blank=True),
        ),
        migrations.AddField(
            model_name='video',
            name='community_artists',
            field=models.ManyToManyField(to='engine.CommunityArtist', blank=True),
        ),
        migrations.AddField(
            model_name='video',
            name='community_songs',
            field=models.ManyToManyField(to='engine.CommunitySong', blank=True),
        ),
    ]
