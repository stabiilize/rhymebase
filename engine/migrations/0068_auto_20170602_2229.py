# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('engine', '0067_userprofile_reddit_username'),
    ]

    operations = [
        migrations.AddField(
            model_name='userprofile',
            name='community_genres',
            field=models.ManyToManyField(to='engine.CommunityGenre', blank=True),
        ),
        migrations.AddField(
            model_name='userprofile',
            name='rhymes',
            field=models.ManyToManyField(to='engine.Rhyme', blank=True),
        ),
    ]
