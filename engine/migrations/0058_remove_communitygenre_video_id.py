# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('engine', '0057_auto_20170530_1758'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='communitygenre',
            name='video_id',
        ),
    ]
