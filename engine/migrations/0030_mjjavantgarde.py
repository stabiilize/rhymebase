# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('engine', '0029_rhyme_parent'),
    ]

    operations = [
        migrations.CreateModel(
            name='MJJAvantGarde',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('first_rhyme', models.ForeignKey(to='engine.Rhyme')),
                ('rhymes', models.ManyToManyField(related_name='rhymes', to='engine.Rhyme')),
            ],
        ),
    ]
