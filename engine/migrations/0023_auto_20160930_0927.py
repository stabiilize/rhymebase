# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('engine', '0022_auto_20160930_0910'),
    ]

    operations = [
        migrations.AlterField(
            model_name='composedvideo',
            name='structure_fallback',
            field=models.TextField(unique=True, null=True),
        ),
    ]
