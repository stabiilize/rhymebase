# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import engine.models


class Migration(migrations.Migration):

    dependencies = [
        ('engine', '0019_auto_20160930_0845'),
    ]

    operations = [
        migrations.AlterField(
            model_name='composedvideo',
            name='structure_fallback',
            field=models.TextField(default='', unique=True, blank=True),
            preserve_default=False,
        ),
    ]
