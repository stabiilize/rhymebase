# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import jsonfield.fields


class Migration(migrations.Migration):

    dependencies = [
        ('engine', '0014_auto_20160927_1616'),
    ]

    operations = [
        migrations.AlterField(
            model_name='composedvideo',
            name='structure',
            field=jsonfield.fields.JSONField(unique=True, null=True),
        ),
        migrations.AlterField(
            model_name='composedvideo',
            name='structure_fallback',
            field=models.TextField(unique=True, blank=True),
        ),
    ]
