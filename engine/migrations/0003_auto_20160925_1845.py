# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import jsonfield.fields


class Migration(migrations.Migration):

    dependencies = [
        ('engine', '0002_auto_20160925_1757'),
    ]

    operations = [
        migrations.AddField(
            model_name='video',
            name='captions_json',
            field=jsonfield.fields.JSONField(null=True, blank=True),
        ),
        migrations.AddField(
            model_name='video',
            name='has_captions',
            field=models.BooleanField(default=False),
        ),
    ]
