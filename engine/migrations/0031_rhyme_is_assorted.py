# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('engine', '0030_mjjavantgarde'),
    ]

    operations = [
        migrations.AddField(
            model_name='rhyme',
            name='is_assorted',
            field=models.BooleanField(default=False),
        ),
    ]
