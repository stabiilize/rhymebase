# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('engine', '0070_auto_20170606_1834'),
    ]

    operations = [
        migrations.AddField(
            model_name='userprofile',
            name='community_artists',
            field=models.ManyToManyField(to='engine.CommunityArtist', blank=True),
        ),
        migrations.AddField(
            model_name='userprofile',
            name='community_songs',
            field=models.ManyToManyField(to='engine.CommunitySong', blank=True),
        ),
    ]
