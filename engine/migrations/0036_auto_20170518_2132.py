# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('engine', '0035_auto_20161201_1515'),
    ]

    operations = [
        migrations.RenameModel(
            old_name='MJJAvantGarde',
            new_name='AssortedRhyme',
        ),
        migrations.RemoveField(
            model_name='createdvideo',
            name='rhymes_used',
        ),
        migrations.RemoveField(
            model_name='rhyme',
            name='parent',
        ),
        migrations.AddField(
            model_name='rhyme',
            name='community_duration',
            field=models.CharField(max_length=20, null=True, blank=True),
        ),
        migrations.AddField(
            model_name='rhyme',
            name='community_full_line',
            field=models.TextField(null=True, blank=True),
        ),
        migrations.AddField(
            model_name='rhyme',
            name='community_last_word',
            field=models.CharField(max_length=50, null=True, blank=True),
        ),
        migrations.AddField(
            model_name='rhyme',
            name='community_start',
            field=models.CharField(max_length=20, null=True, blank=True),
        ),
        migrations.AddField(
            model_name='video',
            name='from_dev',
            field=models.BooleanField(default=True),
        ),
        migrations.DeleteModel(
            name='CreatedVideo',
        ),
    ]
