# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('engine', '0056_communitygenre_video_id'),
    ]

    operations = [
        migrations.AlterField(
            model_name='communitygenre',
            name='genre',
            field=models.CharField(unique=True, max_length=50),
        ),
    ]
