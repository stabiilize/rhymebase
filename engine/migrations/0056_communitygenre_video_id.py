# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('engine', '0055_auto_20170530_1747'),
    ]

    operations = [
        migrations.AddField(
            model_name='communitygenre',
            name='video_id',
            field=models.CharField(max_length=20, null=True, blank=True),
        ),
    ]
