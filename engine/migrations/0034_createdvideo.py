# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('engine', '0033_video_rhymes'),
    ]

    operations = [
        migrations.CreateModel(
            name='CreatedVideo',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('video_path', models.FilePathField(path=b'/media', blank=True)),
                ('rhymes_used', models.ManyToManyField(to='engine.Rhyme')),
            ],
        ),
    ]
