# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('engine', '0041_auto_20170519_2026'),
    ]

    operations = [
        migrations.AddField(
            model_name='communityrhymeduation',
            name='num_vote_down',
            field=models.PositiveIntegerField(default=0, db_index=True),
        ),
        migrations.AddField(
            model_name='communityrhymeduation',
            name='num_vote_up',
            field=models.PositiveIntegerField(default=0, db_index=True),
        ),
        migrations.AddField(
            model_name='communityrhymeduation',
            name='vote_score',
            field=models.IntegerField(default=0, db_index=True),
        ),
        migrations.AddField(
            model_name='communityrhymefullline',
            name='num_vote_down',
            field=models.PositiveIntegerField(default=0, db_index=True),
        ),
        migrations.AddField(
            model_name='communityrhymefullline',
            name='num_vote_up',
            field=models.PositiveIntegerField(default=0, db_index=True),
        ),
        migrations.AddField(
            model_name='communityrhymefullline',
            name='vote_score',
            field=models.IntegerField(default=0, db_index=True),
        ),
        migrations.AddField(
            model_name='communityrhymelastword',
            name='num_vote_down',
            field=models.PositiveIntegerField(default=0, db_index=True),
        ),
        migrations.AddField(
            model_name='communityrhymelastword',
            name='num_vote_up',
            field=models.PositiveIntegerField(default=0, db_index=True),
        ),
        migrations.AddField(
            model_name='communityrhymelastword',
            name='vote_score',
            field=models.IntegerField(default=0, db_index=True),
        ),
        migrations.AddField(
            model_name='communityrhymestart',
            name='num_vote_down',
            field=models.PositiveIntegerField(default=0, db_index=True),
        ),
        migrations.AddField(
            model_name='communityrhymestart',
            name='num_vote_up',
            field=models.PositiveIntegerField(default=0, db_index=True),
        ),
        migrations.AddField(
            model_name='communityrhymestart',
            name='vote_score',
            field=models.IntegerField(default=0, db_index=True),
        ),
    ]
