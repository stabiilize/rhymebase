# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('engine', '0042_auto_20170520_1618'),
    ]

    operations = [
        migrations.AddField(
            model_name='rhyme',
            name='slug',
            field=models.SlugField(null=True, blank=True),
        ),
    ]
