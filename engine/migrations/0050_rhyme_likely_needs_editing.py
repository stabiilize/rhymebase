# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('engine', '0049_auto_20170525_2134'),
    ]

    operations = [
        migrations.AddField(
            model_name='rhyme',
            name='likely_needs_editing',
            field=models.BooleanField(default=False),
        ),
    ]
