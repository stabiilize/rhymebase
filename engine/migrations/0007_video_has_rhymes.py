# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('engine', '0006_video_video_downloaded'),
    ]

    operations = [
        migrations.AddField(
            model_name='video',
            name='has_rhymes',
            field=models.BooleanField(default=False),
        ),
    ]
