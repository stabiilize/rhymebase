# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('engine', '0026_rhyme'),
    ]

    operations = [
        migrations.AddField(
            model_name='rhyme',
            name='full_line',
            field=models.TextField(default=None),
            preserve_default=False,
        ),
    ]
