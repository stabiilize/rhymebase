# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('engine', '0031_rhyme_is_assorted'),
    ]

    operations = [
        migrations.AddField(
            model_name='rhyme',
            name='full_line_raw',
            field=models.TextField(blank=True),
        ),
    ]
