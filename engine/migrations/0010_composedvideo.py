# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import jsonfield.fields


class Migration(migrations.Migration):

    dependencies = [
        ('engine', '0009_video_genre'),
    ]

    operations = [
        migrations.CreateModel(
            name='ComposedVideo',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(max_length=255)),
                ('genre', models.CharField(max_length=100)),
                ('structure', jsonfield.fields.JSONField()),
                ('video_path', models.FilePathField(path=b'/media', blank=True)),
            ],
        ),
    ]
