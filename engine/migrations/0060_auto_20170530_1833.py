# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('engine', '0059_auto_20170530_1828'),
    ]

    operations = [
        migrations.RenameField(
            model_name='communitygenre',
            old_name='genre',
            new_name='genre_object',
        ),
    ]
