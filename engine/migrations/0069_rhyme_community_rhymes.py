# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('engine', '0068_auto_20170602_2229'),
    ]

    operations = [
        migrations.AddField(
            model_name='rhyme',
            name='community_rhymes',
            field=models.ManyToManyField(related_name='_community_rhymes_+', to='engine.Rhyme', blank=True),
        ),
    ]
