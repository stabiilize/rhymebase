# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('engine', '0045_auto_20170524_1338'),
    ]

    operations = [
        migrations.AddField(
            model_name='communityrhyme',
            name='video',
            field=models.ForeignKey(blank=True, to='engine.Video', null=True),
        ),
    ]
