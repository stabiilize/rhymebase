# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('engine', '0043_rhyme_slug'),
    ]

    operations = [
        migrations.AlterField(
            model_name='rhyme',
            name='slug',
            field=models.SlugField(max_length=255, null=True, blank=True),
        ),
    ]
