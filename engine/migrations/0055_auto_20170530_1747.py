# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('engine', '0054_rhyme_is_added_to_video'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='communitygenre',
            name='video_id',
        ),
        migrations.AddField(
            model_name='communitygenre',
            name='videos',
            field=models.ManyToManyField(to='engine.Video', null=True, blank=True),
        ),
    ]
