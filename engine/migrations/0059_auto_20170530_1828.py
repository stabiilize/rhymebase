# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('engine', '0058_remove_communitygenre_video_id'),
    ]

    operations = [
        migrations.CreateModel(
            name='Genre',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('genre', models.CharField(max_length=50)),
            ],
        ),
        migrations.RemoveField(
            model_name='communitygenre',
            name='genre'
        ),
        migrations.AddField(
            model_name='communitygenre',
            name='genre',
            field=models.ForeignKey(to='engine.Genre'),
        ),
    ]
