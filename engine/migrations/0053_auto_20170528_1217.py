# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('engine', '0052_auto_20170528_0229'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='communityrhyme',
            name='video_object',
        ),
        migrations.AddField(
            model_name='communityrhyme',
            name='is_added_to_video',
            field=models.BooleanField(default=False),
        ),
        migrations.AddField(
            model_name='video',
            name='community_rhymes',
            field=models.ManyToManyField(to='engine.CommunityRhyme', null=True, blank=True),
        ),
        migrations.AddField(
            model_name='video',
            name='rhymes',
            field=models.ManyToManyField(to='engine.Rhyme', null=True, blank=True),
        ),
    ]
