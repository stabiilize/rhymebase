# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('engine', '0053_auto_20170528_1217'),
    ]

    operations = [
        migrations.AddField(
            model_name='rhyme',
            name='is_added_to_video',
            field=models.BooleanField(default=False),
        ),
    ]
