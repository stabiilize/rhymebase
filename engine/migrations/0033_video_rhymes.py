# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('engine', '0032_rhyme_full_line_raw'),
    ]

    operations = [
        migrations.AddField(
            model_name='video',
            name='rhymes',
            field=models.ManyToManyField(to='engine.Rhyme', blank=True),
        ),
    ]
