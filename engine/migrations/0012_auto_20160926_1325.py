# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('engine', '0011_auto_20160926_0727'),
    ]

    operations = [
        migrations.AddField(
            model_name='video',
            name='composed_videos',
            field=models.ManyToManyField(to='engine.ComposedVideo', blank=True),
        ),
        migrations.AlterField(
            model_name='composedvideo',
            name='title',
            field=models.CharField(max_length=255),
        ),
    ]
