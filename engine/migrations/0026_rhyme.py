# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('engine', '0025_auto_20160930_1007'),
    ]

    operations = [
        migrations.CreateModel(
            name='Rhyme',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('video_id', models.CharField(max_length=20)),
                ('last_word', models.CharField(max_length=50)),
                ('start', models.CharField(max_length=20)),
                ('duration', models.CharField(max_length=20)),
                ('video_title', models.TextField()),
            ],
        ),
    ]
