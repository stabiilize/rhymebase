# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('engine', '0012_auto_20160926_1325'),
    ]

    operations = [
        migrations.AddField(
            model_name='video',
            name='has_composed_videos',
            field=models.BooleanField(default=False),
        ),
    ]
