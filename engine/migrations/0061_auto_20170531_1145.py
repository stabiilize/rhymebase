# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('engine', '0060_auto_20170530_1833'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='communitygenre',
            name='videos',
        ),
        migrations.AddField(
            model_name='communitygenre',
            name='video_id',
            field=models.CharField(max_length=20, null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='genre',
            name='genre',
            field=models.CharField(unique=True, max_length=50),
        ),
        migrations.AlterField(
            model_name='video',
            name='community_rhymes',
            field=models.ManyToManyField(to='engine.CommunityRhyme', blank=True),
        ),
        migrations.AlterField(
            model_name='video',
            name='rhymes',
            field=models.ManyToManyField(to='engine.Rhyme', blank=True),
        ),
    ]
