# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('engine', '0063_video_community_genres'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='communityrhyme',
            name='original_rhyme',
        ),
        migrations.RemoveField(
            model_name='communityrhyme',
            name='user',
        ),
        migrations.RemoveField(
            model_name='video',
            name='community_rhymes',
        ),
        migrations.AddField(
            model_name='communitygenre',
            name='is_added_to_video',
            field=models.BooleanField(default=False),
        ),
        migrations.AddField(
            model_name='rhyme',
            name='is_community',
            field=models.BooleanField(default=False),
        ),
        migrations.AddField(
            model_name='rhyme',
            name='num_vote_down',
            field=models.PositiveIntegerField(default=0, db_index=True),
        ),
        migrations.AddField(
            model_name='rhyme',
            name='num_vote_up',
            field=models.PositiveIntegerField(default=0, db_index=True),
        ),
        migrations.AddField(
            model_name='rhyme',
            name='original_rhyme',
            field=models.ForeignKey(blank=True, to='engine.Rhyme', null=True),
        ),
        migrations.AddField(
            model_name='rhyme',
            name='vote_score',
            field=models.IntegerField(default=0, db_index=True),
        ),
        migrations.DeleteModel(
            name='CommunityRhyme',
        ),
    ]
