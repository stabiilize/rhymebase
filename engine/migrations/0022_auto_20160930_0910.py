# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import engine.models


class Migration(migrations.Migration):

    dependencies = [
        ('engine', '0021_auto_20160930_0901'),
    ]

    operations = [
        migrations.AlterField(
            model_name='composedvideo',
            name='structure_fallback',
            field=models.TextField(unique=True, blank=True),
        ),
    ]
