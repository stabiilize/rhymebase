# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('engine', '0007_video_has_rhymes'),
    ]

    operations = [
        migrations.AddField(
            model_name='video',
            name='has_possible_rhymes',
            field=models.BooleanField(default=False),
        ),
    ]
