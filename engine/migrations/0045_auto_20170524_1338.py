# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('engine', '0044_auto_20170523_1818'),
    ]

    operations = [
        migrations.CreateModel(
            name='CommunityRhyme',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('vote_score', models.IntegerField(default=0, db_index=True)),
                ('num_vote_up', models.PositiveIntegerField(default=0, db_index=True)),
                ('num_vote_down', models.PositiveIntegerField(default=0, db_index=True)),
                ('start', models.CharField(max_length=20)),
                ('duration', models.CharField(max_length=20)),
                ('full_line', models.TextField()),
                ('last_word', models.CharField(max_length=50)),
                ('date_added', models.DateTimeField(auto_now_add=True)),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.RemoveField(
            model_name='communityrhymeduation',
            name='user',
        ),
        migrations.RemoveField(
            model_name='communityrhymefullline',
            name='user',
        ),
        migrations.RemoveField(
            model_name='communityrhymelastword',
            name='user',
        ),
        migrations.RemoveField(
            model_name='communityrhymestart',
            name='user',
        ),
        migrations.RemoveField(
            model_name='rhyme',
            name='community_durations',
        ),
        migrations.RemoveField(
            model_name='rhyme',
            name='community_full_line',
        ),
        migrations.RemoveField(
            model_name='rhyme',
            name='community_last_word',
        ),
        migrations.RemoveField(
            model_name='rhyme',
            name='community_starts',
        ),
        migrations.RemoveField(
            model_name='video',
            name='has_composed_videos',
        ),
        migrations.RemoveField(
            model_name='video',
            name='has_possible_rhymes',
        ),
        migrations.RemoveField(
            model_name='video',
            name='rhymes',
        ),
        migrations.RemoveField(
            model_name='video',
            name='video_downloaded',
        ),
        migrations.RemoveField(
            model_name='video',
            name='video_path',
        ),
        migrations.AddField(
            model_name='rhyme',
            name='date_added',
            field=models.DateTimeField(auto_now_add=True, null=True),
        ),
        migrations.AddField(
            model_name='rhyme',
            name='video_object',
            field=models.ForeignKey(blank=True, to='engine.Video', null=True),
        ),
        migrations.DeleteModel(
            name='CommunityRhymeDuation',
        ),
        migrations.DeleteModel(
            name='CommunityRhymeFullLine',
        ),
        migrations.DeleteModel(
            name='CommunityRhymeLastWord',
        ),
        migrations.DeleteModel(
            name='CommunityRhymeStart',
        ),
        migrations.AddField(
            model_name='communityrhyme',
            name='original_rhyme',
            field=models.ForeignKey(blank=True, to='engine.Rhyme', null=True),
        ),
        migrations.AddField(
            model_name='communityrhyme',
            name='user',
            field=models.ForeignKey(to=settings.AUTH_USER_MODEL),
        ),
    ]
