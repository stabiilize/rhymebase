# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('engine', '0017_assortedrhymes'),
    ]

    operations = [
        migrations.AddField(
            model_name='assortedrhymes',
            name='rhyme_word',
            field=models.CharField(max_length=50, unique=True, null=True),
        ),
    ]
