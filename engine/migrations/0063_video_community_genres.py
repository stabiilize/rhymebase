# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('engine', '0062_auto_20170531_1210'),
    ]

    operations = [
        migrations.AddField(
            model_name='video',
            name='community_genres',
            field=models.ManyToManyField(to='engine.CommunityGenre', blank=True),
        ),
    ]
