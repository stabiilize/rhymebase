# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('engine', '0047_communitygenre'),
    ]

    operations = [
        migrations.RenameField(
            model_name='communitygenre',
            old_name='video',
            new_name='video_object',
        ),
        migrations.RenameField(
            model_name='communityrhyme',
            old_name='video',
            new_name='video_object',
        ),
        migrations.AddField(
            model_name='communitygenre',
            name='video_id',
            field=models.CharField(max_length=20, null=True, blank=True),
        ),
        migrations.AddField(
            model_name='communityrhyme',
            name='slug',
            field=models.SlugField(null=True, blank=True),
        ),
        migrations.AddField(
            model_name='communityrhyme',
            name='video_id',
            field=models.CharField(max_length=20, null=True, blank=True),
        ),
    ]
