# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('engine', '0027_rhyme_full_line'),
    ]

    operations = [
        migrations.AlterField(
            model_name='rhyme',
            name='full_line',
            field=models.TextField(unique=True),
        ),
    ]
