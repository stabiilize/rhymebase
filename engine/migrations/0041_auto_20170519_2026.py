# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('engine', '0040_auto_20170519_2011'),
    ]

    operations = [
        migrations.AlterField(
            model_name='rhyme',
            name='uniqueness_id',
            field=models.CharField(max_length=255, unique=True, null=True),
        ),
    ]
