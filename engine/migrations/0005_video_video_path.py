# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('engine', '0004_video_caption_retrieval_failed'),
    ]

    operations = [
        migrations.AddField(
            model_name='video',
            name='video_path',
            field=models.FilePathField(path=b'/media', blank=True),
        ),
    ]
