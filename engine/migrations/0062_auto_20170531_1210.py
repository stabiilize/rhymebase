# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('engine', '0061_auto_20170531_1145'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='video',
            name='genre',
        ),
        migrations.AddField(
            model_name='genre',
            name='videos',
            field=models.ManyToManyField(to='engine.Video', blank=True),
        ),
    ]
