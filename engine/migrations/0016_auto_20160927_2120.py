# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('engine', '0015_auto_20160927_2119'),
    ]

    operations = [
        migrations.AlterField(
            model_name='composedvideo',
            name='structure_fallback',
            field=models.TextField(unique=True, null=True),
        ),
    ]
