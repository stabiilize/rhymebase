# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('engine', '0066_auto_20170602_0814'),
    ]

    operations = [
        migrations.AddField(
            model_name='userprofile',
            name='reddit_username',
            field=models.CharField(max_length=50, null=True, blank=True),
        ),
    ]
