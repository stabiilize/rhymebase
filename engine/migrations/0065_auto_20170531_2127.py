# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('engine', '0064_auto_20170531_1520'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='video',
            name='from_dev',
        ),
        migrations.AddField(
            model_name='genre',
            name='slug',
            field=models.SlugField(max_length=255, null=True, blank=True),
        ),
    ]
