# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('engine', '0005_video_video_path'),
    ]

    operations = [
        migrations.AddField(
            model_name='video',
            name='video_downloaded',
            field=models.BooleanField(default=False),
        ),
    ]
