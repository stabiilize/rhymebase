# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import jsonfield.fields


class Migration(migrations.Migration):

    dependencies = [
        ('engine', '0010_composedvideo'),
    ]

    operations = [
        migrations.AlterField(
            model_name='composedvideo',
            name='structure',
            field=jsonfield.fields.JSONField(unique=True),
        ),
        migrations.AlterField(
            model_name='composedvideo',
            name='title',
            field=models.CharField(unique=True, max_length=255),
        ),
    ]
