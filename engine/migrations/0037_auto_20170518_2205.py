# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('engine', '0036_auto_20170518_2132'),
    ]

    operations = [
        migrations.RenameModel(
            old_name='AssortedRhyme',
            new_name='AssortedRhymes',
        ),
    ]
