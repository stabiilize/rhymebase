# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('engine', '0028_auto_20160930_1229'),
    ]

    operations = [
        migrations.AddField(
            model_name='rhyme',
            name='parent',
            field=models.ForeignKey(to='engine.Rhyme', null=True),
        ),
    ]
