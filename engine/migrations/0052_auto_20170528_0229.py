# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('engine', '0051_auto_20170528_0206'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='communitygenre',
            name='video_object',
        ),
        migrations.RemoveField(
            model_name='rhyme',
            name='video_object',
        ),
    ]
