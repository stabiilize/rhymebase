# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('engine', '0072_auto_20170606_2126'),
    ]

    operations = [
        migrations.CreateModel(
            name='Counts',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('rhymes', models.BigIntegerField()),
                ('assorted_rhymes', models.BigIntegerField()),
                ('genres', models.BigIntegerField()),
                ('artists', models.BigIntegerField()),
                ('songs', models.BigIntegerField()),
            ],
        ),
    ]
