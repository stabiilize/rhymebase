# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('engine', '0034_createdvideo'),
    ]

    operations = [
        migrations.DeleteModel(
            name='AssortedRhymes',
        ),
        migrations.RemoveField(
            model_name='video',
            name='composed_videos',
        ),
        migrations.DeleteModel(
            name='ComposedVideo',
        ),
    ]
