# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import jsonfield.fields


class Migration(migrations.Migration):

    dependencies = [
        ('engine', '0013_video_has_composed_videos'),
    ]

    operations = [
        migrations.AddField(
            model_name='composedvideo',
            name='has_structure_fallback',
            field=models.BooleanField(default=False),
        ),
        migrations.AddField(
            model_name='composedvideo',
            name='structure_fallback',
            field=models.TextField(blank=True),
        ),
        migrations.AlterField(
            model_name='composedvideo',
            name='structure',
            field=jsonfield.fields.JSONField(unique=True, blank=True),
        ),
    ]
