# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import engine.models


class Migration(migrations.Migration):

    dependencies = [
        ('engine', '0018_assortedrhymes_rhyme_word'),
    ]

    operations = [
        migrations.AlterField(
            model_name='composedvideo',
            name='structure_fallback',
            field=models.TextField(unique=True, blank=True,),
        ),
    ]
