# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('engine', '0003_auto_20160925_1845'),
    ]

    operations = [
        migrations.AddField(
            model_name='video',
            name='caption_retrieval_failed',
            field=models.BooleanField(default=False),
        ),
    ]
