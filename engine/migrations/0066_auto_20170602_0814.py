# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('engine', '0065_auto_20170531_2127'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='rhyme',
            name='uniqueness_id',
        ),
        migrations.AddField(
            model_name='rhyme',
            name='user',
            field=models.ForeignKey(blank=True, to=settings.AUTH_USER_MODEL, null=True),
        ),
    ]
