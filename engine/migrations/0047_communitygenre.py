# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('engine', '0046_communityrhyme_video'),
    ]

    operations = [
        migrations.CreateModel(
            name='CommunityGenre',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('vote_score', models.IntegerField(default=0, db_index=True)),
                ('num_vote_up', models.PositiveIntegerField(default=0, db_index=True)),
                ('num_vote_down', models.PositiveIntegerField(default=0, db_index=True)),
                ('genre', models.CharField(max_length=50)),
                ('date_added', models.DateTimeField(auto_now_add=True)),
                ('user', models.ForeignKey(to=settings.AUTH_USER_MODEL)),
                ('video', models.ForeignKey(to='engine.Video')),
            ],
            options={
                'abstract': False,
            },
        ),
    ]
