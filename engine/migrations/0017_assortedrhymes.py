# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import jsonfield.fields


class Migration(migrations.Migration):

    dependencies = [
        ('engine', '0016_auto_20160927_2120'),
    ]

    operations = [
        migrations.CreateModel(
            name='AssortedRhymes',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('structure', jsonfield.fields.JSONField(unique=True, null=True)),
                ('has_structure_fallback', models.BooleanField(default=False)),
                ('structure_fallback', models.TextField(unique=True, null=True)),
            ],
        ),
    ]
