# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import picklefield.fields


class Migration(migrations.Migration):

    dependencies = [
        ('engine', '0023_auto_20160930_0927'),
    ]

    operations = [
        migrations.AlterField(
            model_name='composedvideo',
            name='structure_fallback',
            field=picklefield.fields.PickledObjectField(unique=True, null=True, editable=False),
        ),
    ]
