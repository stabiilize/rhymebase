# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('engine', '0071_auto_20170606_1911'),
    ]

    operations = [
        migrations.AddField(
            model_name='communityartist',
            name='num_vote_down',
            field=models.PositiveIntegerField(default=0, db_index=True),
        ),
        migrations.AddField(
            model_name='communityartist',
            name='num_vote_up',
            field=models.PositiveIntegerField(default=0, db_index=True),
        ),
        migrations.AddField(
            model_name='communityartist',
            name='vote_score',
            field=models.IntegerField(default=0, db_index=True),
        ),
        migrations.AddField(
            model_name='communitysong',
            name='num_vote_down',
            field=models.PositiveIntegerField(default=0, db_index=True),
        ),
        migrations.AddField(
            model_name='communitysong',
            name='num_vote_up',
            field=models.PositiveIntegerField(default=0, db_index=True),
        ),
        migrations.AddField(
            model_name='communitysong',
            name='vote_score',
            field=models.IntegerField(default=0, db_index=True),
        ),
    ]
