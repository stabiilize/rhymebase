#!/usr/bin/python

import httplib2
import os
import sys
import urllib2
import json
import random
import re

from apiclient.discovery import build
from oauth2client.client import flow_from_clientsecrets
from oauth2client.file import Storage
from oauth2client.tools import argparser, run_flow

sys.path.append("C:\\Users\\Mike\\projects\\mjjavantgarde\\mjavantgarde")
os.environ["DJANGO_SETTINGS_MODULE"] = "mjavantgarde.settings"

from engine.models import Video


CLIENT_SECRETS_FILE = "client_secrets.json"

MISSING_CLIENT_SECRETS_MESSAGE = """
WARNING: Please configure OAuth 2.0

To make this sample run you will need to populate the client_secrets.json file
found at:

   %s

with information from the Developers Console
https://console.developers.google.com/

For more information about the client_secrets.json file format, please visit:
https://developers.google.com/api-client-library/python/guide/aaa_client_secrets
""" % os.path.abspath(os.path.join(os.path.dirname(__file__),
								   CLIENT_SECRETS_FILE))

# This OAuth 2.0 access scope allows for read-only access to the authenticated
# user's account, but not other types of account access.
YOUTUBE_READONLY_SCOPE = "https://www.googleapis.com/auth/youtube.readonly"
YOUTUBE_API_SERVICE_NAME = "youtube"
YOUTUBE_API_VERSION = "v3"

API_KEY = "AIzaSyDntKKATNzobtgBK9xBeFgBkiE0h5BzbEI"

flow = flow_from_clientsecrets(CLIENT_SECRETS_FILE,
	message=MISSING_CLIENT_SECRETS_MESSAGE,
	scope=YOUTUBE_READONLY_SCOPE)

storage = Storage("%s-oauth2.json" % sys.argv[0])
credentials = storage.get()

if credentials is None or credentials.invalid:
	flags = argparser.parse_args()
	credentials = run_flow(flow, storage, flags)

youtube = build(YOUTUBE_API_SERVICE_NAME, YOUTUBE_API_VERSION,
	http=credentials.authorize(httplib2.Http()))

# Retrieve the contentDetails part of the channel resource for the
# authenticated user's channel.
#channels_response = youtube.channels().list(
#  mine=True,
#  part="contentDetails"
#).execute()

#for channel in channels_response["items"]:
  # From the API response, extract the playlist ID that identifies the list
  # of videos uploaded to the authenticated user's channel.
uploads_list_id = 'PLqYXv_L7NiEyYnfZhVHR7ixOTANxjes89'

print "Videos in list %s" % uploads_list_id

got_playlists = []
def create_videos_object(playlist_id):
	got_playlists.append(playlist_id)
	  # Retrieve the list of videos uploaded to the authenticated user's channel.
	playlistitems_list_request = youtube.playlistItems().list(
		playlistId=playlist_id,
		part="snippet",
		maxResults=50
	)

	while playlistitems_list_request:
		playlistitems_list_response = playlistitems_list_request.execute()
		next_id_set = False
		random_int = random.randint(0,(len(playlistitems_list_response["items"])-1))
		# Print information about each video.
		for i,playlist_item in enumerate(playlistitems_list_response["items"]):
			
			if (i == random_int) and not next_id_set:
				
				query_string = playlist_item['snippet']['title']
				query_string = query_string.replace(' ','+')
				query_string = re.sub('[^A-Za-z0-9+]+', '', query_string)
				query_url = "https://www.googleapis.com/youtube/v3/search?part=id,snippet&maxResults=20&q=%s&type=playlist&key=%s" % (query_string, API_KEY)
				p_response = urllib2.urlopen(query_url)
				p_data = json.load(p_response)
				def get_random_playlist():
					randint2 = random.randint(0,(len(p_data["items"])-1))
					if p_data['items'][randint2] not in got_playlists:
						next_id = p_data['items'][randint2]['id']['playlistId']
						next_id_set = True
						print 'NEW PLAYLIST ID: %s' % (next_id)
						return
					else:
						get_random_playlist()
				get_random_playlist()
				
			try:
				title = playlist_item["snippet"]["title"]
				video_id = playlist_item["snippet"]["resourceId"]["videoId"]

				vquery_url = "https://www.googleapis.com/youtube/v3/videos?id=%s&part=contentDetails&key=%s" % (video_id,API_KEY)
				response = urllib2.urlopen(vquery_url)
				data = json.load(response) 
				caption = data["items"][0][u"contentDetails"][u"caption"]
				duration = data["items"][0][u"contentDetails"][u"duration"]


				if caption == "true":
					try:
						print "%s (%s) has captions, saving video" % (title, video_id)

						Video.objects.create(
							title=title,
							video_id=video_id,
							duration=duration)
					except Exception,e:
						print 'error: %s' % (str(e))
				else:
					pass
					#print "%s (%s) has no captions, skipping" % (title, video_id)
			except:
				pass

		playlistitems_list_request = youtube.playlistItems().list_next(
			playlistitems_list_request, playlistitems_list_response)

	create_videos_object(next_id)

create_videos_object(uploads_list_id)