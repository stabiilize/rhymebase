import re

from django import forms
from django.contrib.auth.models import User
from django.template.defaultfilters import slugify
 
from .models import Video
from .models import Rhyme
from .models import Genre
from .models import Artist
from .models import Song
from .models import CommunityGenre
from .models import CommunityArtist
from .models import CommunitySong
from .models import UserProfile

from captcha.fields import CaptchaField

# NOTICE
# API_KEY USES "mikejohnsonjr" GOOGLE APP, __NOT__ "rhymebase" GOOGLE APP
# also in main.py, engine.views, and engine.models

API_KEY = "AIzaSyDntKKATNzobtgBK9xBeFgBkiE0h5BzbEI"

class RhymeForm(forms.ModelForm):
	captcha = CaptchaField()
	class Meta:
		model = Rhyme
		exclude = ('community_rhymes','uniqueness_id','video_title','full_line_raw','likely_needs_editing','slug','is_community','date_added','user','is_assorted','is_added_to_video','num_vote_up','num_vote_down','original_rhyme','vote_score','last_word',)

class CommunityGenreForm(forms.ModelForm):
	genre = forms.CharField(max_length=50)
	captcha = CaptchaField()
	class Meta:
		model = CommunityGenre
		exclude = ('user','num_vote_down','num_vote_up','vote_score','genre_object','is_added_to_video','video_title')

	def save(self, commit=True):

		genre_name = slugify(self.cleaned_data['genre'])
		genre = Genre.objects.get_or_create(genre=genre_name)[0] # returns (instance, <created?-boolean>)
		self.instance.genre_object = genre

		video_id = self.cleaned_data['video_id']
		if not Video.objects.filter(video_id=video_id).exists():
			url = "https://www.googleapis.com/youtube/v3/videos?id=%s&part=snippet,contentDetails&key=%s" % (video_id,API_KEY)
			response = urllib2.urlopen(url)
			data = json.load(response) 
			title = data["items"][0][u"snippet"][u"title"]
			video_object = Video.objects.create(video_id=video_id,title=title)
		else:
			video_object = Video.objects.get(video_id=video_id)
		
		genre.videos.add(video_object)

		return super(CommunityGenreForm,self).save(commit)

class CommunityArtistForm(forms.ModelForm):
	artist = forms.CharField(max_length=50)
	captcha = CaptchaField()
	class Meta:
		model = CommunityArtist
		exclude = ('user','num_vote_down','num_vote_up','vote_score','artist_object','is_added_to_video','video_title')

	def save(self, commit=True):

		artist_name = slugify(self.cleaned_data['artist'])
		artist = Artist.objects.get_or_create(artist=artist_name)[0] # returns (instance, <created?-boolean>)
		self.instance.artist_object = artist

		video_id = self.cleaned_data['video_id']
		if not Video.objects.filter(video_id=video_id).exists():
			url = "https://www.googleapis.com/youtube/v3/videos?id=%s&part=snippet,contentDetails&key=%s" % (video_id,API_KEY)
			response = urllib2.urlopen(url)
			data = json.load(response) 
			title = data["items"][0][u"snippet"][u"title"]
			video_object = Video.objects.create(video_id=video_id,title=title)
		else:
			video_object = Video.objects.get(video_id=video_id)
		
		artist.videos.add(video_object)

		return super(CommunityArtistForm,self).save(commit)

class CommunitySongForm(forms.ModelForm):
	song = forms.CharField(max_length=50)
	captcha = CaptchaField()
	class Meta:
		model = CommunitySong
		exclude = ('user','num_vote_down','num_vote_up','vote_score','song_object','is_added_to_video','video_title')

	def save(self, commit=True):

		song_name = slugify(self.cleaned_data['song'])
		song = Song.objects.get_or_create(song=song_name)[0] # returns (instance, <created?-boolean>)
		self.instance.song_object = song

		video_id = self.cleaned_data['video_id']
		if not Video.objects.filter(video_id=video_id).exists():
			url = "https://www.googleapis.com/youtube/v3/videos?id=%s&part=snippet,contentDetails&key=%s" % (video_id,API_KEY)
			response = urllib2.urlopen(url)
			data = json.load(response) 
			title = data["items"][0][u"snippet"][u"title"]
			video_object = Video.objects.create(video_id=video_id,title=title)
		else:
			video_object = Video.objects.get(video_id=video_id)
		
		song.videos.add(video_object)

		return super(CommunitySongForm,self).save(commit)

class UserProfileForm(forms.ModelForm):
	captcha = CaptchaField()
	class Meta:
		model = UserProfile
		exclude = ('user','rhymes','community_genres','community_artists','community_songs',)

class DeactivateUserForm(forms.ModelForm):
	class Meta:
		model = User
		fields = ['is_active']

	def __init__(self, *args, **kwargs):
		super(DeactivateUserForm, self).__init__(*args, **kwargs)
		self.fields['is_active'].help_text = "Check this box if you are sure you want to delete this account."

	def clean_is_active(self):  
		# Reverses true/false for your form prior to validation
		#
		# You can also raise a ValidationError here if you receive 
		# a value you don't want, to prevent the form's is_valid 
		# method from return true if, say, the user hasn't chosen 
		# to deactivate their account
		is_active = not(self.cleaned_data["is_active"])
		return is_active