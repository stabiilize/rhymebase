import urllib2
import json
import operator

from django import forms
from django.http import HttpResponse, HttpResponseNotFound
from django.shortcuts import render, HttpResponseRedirect
from django.core.urlresolvers import reverse,reverse_lazy
from django.core.exceptions import PermissionDenied
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
from django.views.generic import ListView
from django.views.generic import DetailView
from django.views.generic.edit import UpdateView
from django.views.generic.edit import CreateView
from django.views.generic.edit import DeleteView
from django.forms.utils import ErrorList
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.contrib.postgres.search import SearchVector

from .models import Video
from .models import Genre
from .models import CommunityGenre
from .models import CommunityArtist
from .models import CommunitySong
from .models import Rhyme
from .models import AssortedRhymes
from .models import UserProfile
from .models import Artist
from .models import Song
from .models import Counts

from .forms import CommunityGenreForm
from .forms import CommunityArtistForm
from .forms import CommunitySongForm
from .forms import RhymeForm
from .forms import UserProfileForm
from .forms import DeactivateUserForm

from watson import search as watson

# NOTICE
# API_KEY USES "mikejohnsonjr" GOOGLE APP, __NOT__ "rhymebase" GOOGLE APP
# also in main.py, engine.forms, and engine.models

API_KEY = "AIzaSyDntKKATNzobtgBK9xBeFgBkiE0h5BzbEI"

def home(request):
	counts = Counts.objects.get(pk=1)
	context = {
		'rhymes_count':counts.rhymes,
		'assorted_rhymes_count':counts.assorted_rhymes,
		'genres_count':counts.genres,
		'artists_count':counts.artists,
		'songs_count':counts.songs,
	}

	template = 'home.html'
	return render(request,template,context)

def search(request):
	q = request.GET.get('q','None')
	_type = request.GET.get('type','None')
	

	types = {
		'rhyme': Rhyme,
		'genre': Genre,
		'artist': Artist,
		'song': Song,
	}


	if _type not in types:
		return render(request,'engine/rhyme_list.html',{'results':[]})

	if _type == 'rhyme':
		results = Rhyme.objects.annotate(
			search=SearchVector('full_line','video_title'),
			).filter(search=q)
	elif _type == 'genre':
		results = Genre.objects.filter(genre__search=q)
	elif _type == 'artist':
		results = Artist.objects.filter(artist__search=q)
	elif _type == 'song':
		results = Song.objects.filter(song__search=q)

	paginator = Paginator(results, 25)
	page = request.GET.get('page')
	try:
		results = paginator.page(page)
	except PageNotAnInteger:
		results = paginator.page(1)
	except EmptyPage:
		results = paginator.page(paginator.num_pages)

	context = {
		'results':results,
		'search_type_key': _type
	}
	template = 'engine/rhyme_list.html'
	return render(request,template,context)

def video(request,video_id):
	context = {}
	url = "https://www.googleapis.com/youtube/v3/videos?id=%s&part=snippet,contentDetails&key=%s" % (video_id,API_KEY)
	response = urllib2.urlopen(url)
	data = json.load(response) 
	try:
		title = data["items"][0][u"snippet"][u"title"]
		thumbnail = data["items"][0][u"snippet"][u"thumbnails"][u"standard"][u"url"]
	except:
		return HttpResponseNotFound('Not Found')


	context['title'] = title
	context['video_id'] = video_id

	if not Video.objects.filter(video_id=video_id).exists():
		video = Video.objects.create(video_id=video_id,title=title,thumbnail_link=thumbnail)
	else:
		video = Video.objects.get(video_id=video_id)

	all_rhymes = video.rhymes.all()
	all_rhymes = sorted(all_rhymes, key=lambda k: float(k.start))
	for r in all_rhymes:
		if not r.original_rhyme == None and r.is_community:
			rhyme_list = list(r.community_rhymes.all()).append(r.original_rhyme)
			chosen_rhyme = r.original_rhyme.get_community_rhyme()
			rhyme_list.pop(chosen_rhyme)
			for rr in rhyme_list:
				all_rhymes.pop(rr)

	context['genre'] = video.get_community_genre()
	context['artist'] = video.get_community_artist()
	context['song'] = video.get_community_song()
	context['all_rhymes'] = all_rhymes
	context['thumbnail'] = thumbnail

	template = 'engine/video.html'
	return render(request,template,context)

def profile(request,slug):

	user_object = User.objects.get(username=slug)
	rhymes = user_object.userprofile.rhymes.all()
	genres = user_object.userprofile.community_genres.all()
	artists = user_object.userprofile.community_artists.all()
	songs = user_object.userprofile.community_songs.all()
	karma = 0

	for r in rhymes:
		karma += r.votes.count()
	for g in genres:
		karma += g.votes.count()
	for a in artists:
		karma += a.votes.count()
	for s in songs:
		karma += s.votes.count()

	karma += (len(Rhyme.objects.all()[0].votes.all(user_object.id))/5.0)
	rhymes_upvoted = Rhyme.objects.all()[0].votes.all(user_object.id)
	genres_upvoted = []
	artists_upvoted = []
	songs_upvoted = []

	if len(CommunityGenre.objects.all()) > 0:
		karma += (len(CommunityGenre.objects.all()[0].votes.all(user_object.id))/5.0)
		genres_upvoted = CommunityGenre.objects.all()[0].votes.all(user_object.id)

	if len(CommunityArtist.objects.all()) > 0:
		karma += (len(CommunityArtist.objects.all()[0].votes.all(user_object.id))/5.0)
		artists_upvoted = CommunityArtist.objects.all()[0].votes.all(user_object.id)

	if len(CommunitySong.objects.all()) > 0:
		karma += (len(CommunitySong.objects.all()[0].votes(user_object.id))/5.0)
		songs_upvoted = CommunitySong.objects.all()[0].votes(user_object.id)

	context = {
		'user_object':user_object,
		'status': user_object.userprofile.status,
		'rhymes': rhymes,
		'genres':genres,
		'artists':artists,
		'songs':songs,
		'rhymes_upvoted': rhymes_upvoted,
		'genres_upvoted': genres_upvoted,
		'artists_upvoted': artists_upvoted,
		'songs_upvoted': songs_upvoted,
		'karma': format(karma,'.2f'),
	}
	template = 'user_profile.html'
	return render(request,template,context)	

def genre(request,slug):
	genre = Genre.objects.get(genre=slug)
	videos_in_genre = genre.get_picked_videos()
	paginator = Paginator(videos_in_genre, 50)
	page = request.GET.get('page')
	try:
		videos_in_genre = paginator.page(page)
	except PageNotAnInteger:
		videos_in_genre = paginator.page(1)
	except EmptyPage:
		videos_in_genre = paginator.page(paginator.num_pages)

	context = {
		'content': videos_in_genre
	}
	template = 'engine/genre.html'
	return render(request, template, context)

def artist(request,slug):
	artist = Artist.objects.get(artist=slug)
	videos_by_artist = artist.get_picked_videos()
	paginator = Paginator(videos_by_artist, 50)
	page = request.GET.get('page')
	try:
		videos_by_artist = paginator.page(page)
	except PageNotAnInteger:
		videos_by_artist = paginator.page(1)
	except EmptyPage:
		videos_by_artist = paginator.page(paginator.num_pages)
	context = {
		'content': videos_by_artist
	}
	template = 'engine/genre.html'
	return render(request, template, context)

def song(request,slug):
	song = Song.objects.get(song=slug)
	videos_of_song = song.get_picked_videos()
	paginator = Paginator(videos_of_song, 50)
	page = request.GET.get('page')
	try:
		videos_of_song = paginator.page(page)
	except PageNotAnInteger:
		videos_of_song = paginator.page(1)
	except EmptyPage:
		videos_of_song = paginator.page(paginator.num_pages)
	context = {
		'content': videos_of_song
	}
	template = 'engine/genre.html'
	return render(request, template, context)

def about(request):
	context = {}
	template = 'engine/about.html'
	return render(request,template,context)

def terms(request):
	context = {}
	template = 'engine/terms.html'
	return render(request,template,context)

def faq(request):
	context = {}
	template = 'engine/faq.html'
	return render(request,template,context)

def likely_needs_edits(request):
	context = {}
	template = 'likely_needs_edits.html'
	return render(request,template,context)

class RhymeDetailView(DetailView): #FormMixin
	model = Rhyme

	def get_success_url(self):
		return reverse("rhyme_detail", kwargs={
			'video_id':str(self.kwargs['video_id']),
			'pk':str(self.kwargs['pk']),
			'slug':str(self.kwargs['slug'])})

	def get_context_data(self,**kwargs):

		context = super(RhymeDetailView, self).get_context_data(**kwargs)
		context['item'] = Rhyme.objects.get(pk=self.kwargs['pk'])

		video_id = self.kwargs['video_id']

		all_rhymes = Video.objects.get(video_id=video_id).rhymes.all()
		all_rhymes = sorted(all_rhymes, key=lambda k: float(k.start))
		for r in all_rhymes:
			if not r.original_rhyme == None and r.is_community:
				rhyme_list = list(r.community_rhymes.all()).append(r.original_rhyme)
				chosen_rhyme = r.original_rhyme.get_community_rhyme()
				rhyme_list.pop(chosen_rhyme)
				for rr in rhyme_list:
					all_rhymes.pop(rr)

		starts_and_ends = [[r.start,(float(r.start)+float(r.duration))] for r in all_rhymes]

		video = Video.objects.get(video_id=video_id)
		context['all_rhymes'] = all_rhymes
		context['starts_and_ends_and_captions'] = zip(starts_and_ends,all_rhymes)
		context['genre'] = video.get_community_genre()
		context['artist'] = video.get_community_artist()
		context['song'] = video.get_community_song()
		context['thumbnail'] = 'https://i.ytimg.com/vi/%s/hqdefault.jpg' % (video_id,)

		rhyme = Rhyme.objects.get(pk=self.kwargs['pk'])
		if AssortedRhymes.objects.filter(rhymes=rhyme).exists():
			assorted_rhymes = AssortedRhymes.objects.filter(rhymes=rhyme)[0].rhymes.all()
			rhyme_group = AssortedRhymes.objects.filter(rhymes=rhyme)[0].first_rhyme.last_word
		elif AssortedRhymes.objects.filter(first_rhyme=rhyme).exists():
			assorted_rhymes = AssortedRhymes.objects.filter(first_rhyme=rhyme)[0].rhymes.all()
			rhyme_group = rhyme.last_word
		else:
			assorted_rhymes = None
			rhyme_group = None

		if assorted_rhymes != None:
			video_ids = [ar.video_id for ar in assorted_rhymes]
			video_objects = [Video.objects.get(video_id=v) for v in video_ids]
			assorted_videos = [v.title for v in video_objects]
			assorted_rhymes_and_videos = zip(assorted_rhymes,assorted_videos)

			paginator = Paginator(assorted_rhymes_and_videos, 50)
			page = self.request.GET.get('page')
			try:
				assorted_rhymes_and_videos = paginator.page(page)
			except PageNotAnInteger:
				assorted_rhymes_and_videos = paginator.page(1)
			except EmptyPage:
				assorted_rhymes_and_videos = paginator.page(paginator.num_pages)
		else:
			assorted_rhymes_and_videos = None

		context['assorted_rhymes_and_videos'] = assorted_rhymes_and_videos
		context['rhyme_group'] = rhyme_group

		return context

class RhymeCreateView(CreateView):
	model = Rhyme
	form_class = RhymeForm
	template_name = 'engine/community_rhyme_create.html'

	def form_valid(self,form):
		form = RhymeForm(self.request.POST)
		f = form.save(commit=False)
		f.user = self.request.user
		f.is_community = True

		counts = Counts.objects.get(pk=1)
		counts.rhymes += 1
		counts.save()

		f.save()
		return super(RhymeCreateView,self).form_valid(form)

class RhymeDeleteView(DeleteView):
	model = Rhyme
	success_url = reverse_lazy('home')

	def form_valid(self,form):
		f = form.save(commit=False)
		counts = Counts.objects.get(pk=1)
		counts.rhymes -= 1
		counts.save()
		return super(RhymeDeleteView,self).form_valid(form)

class CommunityGenreCreateView(CreateView):
	model = CommunityGenre
	form_class = CommunityGenreForm
	template_name = 'engine/community_genre_create.html'

	def get_context_data(self,**kwargs):
		context = super(CommunityGenreCreateView,self).get_context_data(**kwargs)
		context['video_id'] = self.kwargs['video_id']
		return context

	def form_valid(self,form):
		f = form.save(commit=False)
		try:
			v = Video.objects.get(video_id=f.video_id)
		except:
			url = "https://www.googleapis.com/youtube/v3/videos?id=%s&part=snippet,contentDetails&key=%s" % (f.video_id,API_KEY)
			response = urllib2.urlopen(url)
			data = json.load(response) 
			try:
				title = data["items"][0][u"snippet"][u"title"]
				thumbnail = data["items"][0][u"snippet"][u"thumbnails"][u"standard"][u"url"]
			except:
				return HttpResponseNotFound('Not Found')

			v = Video.objects.create(
				video_id=f.video_id,
				title=title,
				thumbnail=thumbnail)

		if v.community_genres.filter(user=self.request.user).exists():
			form._errors[forms.forms.NON_FIELD_ERRORS] = ErrorList([
                    u'You have already defined this video\'s genre. Please delete the genre object you created if you\'d like to redefine this video\'s genre.'
                ])
			return self.form_invalid(form)

		f.user = self.request.user
		f.video_title = v.title

		counts = Counts.objects.get(pk=1)
		counts.genres += 1
		counts.save()

		f.save()
		return super(CommunityGenreCreateView,self).form_valid(form)

class CommunityGenreDeleteView(DeleteView):
	model = CommunityGenre
	success_url = reverse_lazy('home')

	def form_valid(self,form):
		f = form.save(commit=False)
		counts = Counts.objects.get(pk=1)
		counts.genres -= 1
		counts.save()
		return super(CommunityGenreDeleteView,self).form_valid(form)

class CommunityArtistCreateView(CreateView):
	model = CommunityArtist
	form_class = CommunityArtistForm
	template_name = 'engine/community_artist_create.html'

	def form_valid(self,form):
		f = form.save(commit=False)
		try:
			v = Video.objects.get(video_id=f.video_id)
		except:
			url = "https://www.googleapis.com/youtube/v3/videos?id=%s&part=snippet,contentDetails&key=%s" % (f.video_id,API_KEY)
			response = urllib2.urlopen(url)
			data = json.load(response) 
			try:
				title = data["items"][0][u"snippet"][u"title"]
				thumbnail = data["items"][0][u"snippet"][u"thumbnails"][u"standard"][u"url"]
			except:
				return HttpResponseNotFound('Not Found')

			v = Video.objects.create(
				video_id=f.video_id,
				title=title,
				thumbnail=thumbnail)

		if v.community_artists.filter(user=self.request.user).exists():
			form._errors[forms.forms.NON_FIELD_ERRORS] = ErrorList([
                    u'You have already defined this video\'s artist. Please delete the artist object you created if you\'d like to redefine this video\'s artist.'
                ])
			return self.form_invalid(form)
		f.user = self.request.user
		f.video_title = v.title

		f.save()
		return super(CommunityArtistCreateView,self).form_valid(form)

class CommunityArtistDeleteView(DeleteView):
	model = CommunityArtist
	success_url = reverse_lazy('home')

	def form_valid(self,form):
		f = form.save(commit=False)
		counts = Counts.objects.get(pk=1)
		counts.artists -= 1
		counts.save()
		return super(CommunityArtistDeleteView,self).form_valid(form)

class CommunitySongCreateView(CreateView):
	model = CommunitySong
	form_class = CommunitySongForm
	template_name = 'engine/community_song_create.html'

	def form_valid(self,form):
		f = form.save(commit=False)
		try:
			v = Video.objects.get(video_id=f.video_id)
		except:
			url = "https://www.googleapis.com/youtube/v3/videos?id=%s&part=snippet,contentDetails&key=%s" % (f.video_id,API_KEY)
			response = urllib2.urlopen(url)
			data = json.load(response) 
			try:
				title = data["items"][0][u"snippet"][u"title"]
				thumbnail = data["items"][0][u"snippet"][u"thumbnails"][u"standard"][u"url"]
			except:
				return HttpResponseNotFound('Not Found')

			v = Video.objects.create(
				video_id=f.video_id,
				title=title,
				thumbnail=thumbnail)

		if v.community_songs.filter(user=self.request.user).exists():
			form._errors[forms.forms.NON_FIELD_ERRORS] = ErrorList([
                    u'You have already defined this video\'s song. Please delete the song object you created if you\'d like to redefine this video\'s genre.'
                ])
			return self.form_invalid(form)
		f.user = self.request.user
		f.video_title = v.title

		counts = Counts.objects.get(pk=1)
		counts.songs += 1
		counts.save()

		f.save()
		return super(CommunitySongCreateView,self).form_valid(form)

class CommunitySongDeleteView(DeleteView):
	model = CommunitySong
	success_url = reverse_lazy('home')

	def form_valid(self,form):
		f = form.save(commit=False)
		counts = Counts.objects.get(pk=1)
		counts.songs -= 1
		counts.save()
		return super(CommunitySongDeleteView,self).form_valid(form)

@login_required
def deactivate_user(request):

	pk = request.user.id
	user = User.objects.get(pk=pk)
	status = user.userprofile.status
	template = 'account/userprofile_del.html'
	form = DeactivateUserForm(instance=user)
	context = {
		'form':form
	}
	if user.is_authenticated() and request.user.id == user.id:
		if request.method == 'POST':
			user_form = DeactivateUserForm(request.POST, instance=user)
			if user_form.is_valid():
				deactivate_user = user_form.save(commit=False)
				user.is_active = False
				deactivate_user.save()
				return HttpResponseRedirect(reverse_lazy('account_logout'))
		return render(request,template,context) 
	else:
		raise PermissionDenied

class UserProfileEditView(UpdateView):
	model = UserProfile
	form_class = UserProfileForm
	template_name = "account/edit_profile.html"

	def get_object(self, queryset=None):
		return UserProfile.objects.get_or_create(user=self.request.user)[0]

	def get_success_url(self):
		return reverse("user_profile", kwargs={"slug": self.request.user})