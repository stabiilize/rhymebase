import os
import sys

import urllib2
import xmltodict
import json

sys.path.append("C:\\Users\\Mike\\projects\\mjjavantgarde\\mjavantgarde")
os.environ["DJANGO_SETTINGS_MODULE"] = "mjavantgarde.settings"

from engine.models import Video

video_list = Video.objects.filter(has_captions=False)
for v in video_list:
	if not v.caption_retrieval_failed:
		print 'getting captions for %s' % (v.title) 
		url = 'http://video.google.com/timedtext?lang=en&v=%s' % (v.video_id)

		try:
			response = urllib2.urlopen(url)
		except Exception,e:
			print 'error: %s' % (str(e))
			
		print 'parsing captions for %s' % (v.title)
		try:
			captions = xmltodict.parse(response)
			print 'saving captions'
			v.captions_json = captions
			v.has_captions = True
			v.save()
		except:
			v.caption_retrieval_failed = True
			print 'caption retrieval for %s failed' % (v.title)
			v.save()
