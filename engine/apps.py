from django.apps import AppConfig
from watson import search as watson

class EngineConfig(AppConfig):
    name = "engine"
    def ready(self):
        Rhyme = self.get_model("Rhyme")
        watson.register(Rhyme)

        Genre = self.get_model("Genre")
        watson.register(Genre)

        Song = self.get_model("Song")
        watson.register(Song)

        Artist = self.get_model("Artist")
        watson.register(Artist)