"""mjavantgarde URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.8/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Add an import:  from blog import urls as blog_urls
    2. Add a URL to urlpatterns:  url(r'^blog/', include(blog_urls))
"""
from django.conf.urls import include, url
from django.contrib.auth.decorators import login_required
from django.contrib import admin

from django.contrib.auth.models import User

from engine.views import home
from engine.views import search
from engine.views import video

from engine.views import genre
from engine.views import artist
from engine.views import song

from engine.views import about
from engine.views import faq
from engine.views import terms

from engine.views import likely_needs_edits

from engine.views import profile
from engine.views import deactivate_user

from engine.views import RhymeDetailView

from engine.views import RhymeCreateView
from engine.views import RhymeDetailView
from engine.views import RhymeDeleteView

from engine.views import CommunityGenreCreateView
from engine.views import CommunityGenreDeleteView
from engine.views import CommunityArtistCreateView
from engine.views import CommunityArtistDeleteView
from engine.views import CommunitySongCreateView
from engine.views import CommunitySongDeleteView

from engine.views import UserProfileEditView

urlpatterns = [
    url(r'^$', home, name='home'),

    url(r'^api/', include('api.urls')),

    url(r'^search/$',search, name='search'),

    url(r'^about/$',about, name='about'),
    url(r'^faq/$',faq, name='faq'),
    url(r'^terms/$',terms, name='terms'),

    url(r'^likely_needs_edits/$',likely_needs_edits, name='likely_needs_edits'),

    url(r'^genre/(?P<slug>[-\w]+)/$',genre, name='genre'),
    url(r'^artist/(?P<slug>[-\w]+)/$',artist, name='artist'),
    url(r'^song/(?P<slug>[-\w]+)/$',song, name='song'),

    url(r'^(?P<video_id>[-\w]+)/r/(?P<pk>\d+)/(?P<slug>[-\w]+)/$',RhymeDetailView.as_view(),name='rhyme_detail'),

    url(r'^(?P<video_id>[-\w]+)/r/create/$',login_required(RhymeCreateView.as_view()),name='community_rhyme_create'),
    url(r'^(?P<video_id>[-\w]+)/r/delete/(?P<pk>\d+)/(?P<slug>[-\w]+)/$',login_required(RhymeDeleteView.as_view()),name='community_rhyme_delete'),

    url(r'^(?P<video_id>[-\w]+)/genre/define/$',login_required(CommunityGenreCreateView.as_view()),name='community_genre_create'),
    url(r'^(?P<video_id>[-\w]+)/genre/delete/(?P<pk>\d+)/(?P<slug>[-\w]+)/$',login_required(CommunityGenreDeleteView.as_view()),name='community_genre_delete'),

    url(r'^(?P<video_id>[-\w]+)/artist/define/$',login_required(CommunityArtistCreateView.as_view()),name='community_artist_create'),
    url(r'^(?P<video_id>[-\w]+)/artist/delete/(?P<pk>\d+)/(?P<slug>[-\w]+)/$',login_required(CommunityArtistDeleteView.as_view()),name='community_artist_delete'),

    url(r'^(?P<video_id>[-\w]+)/song/define/$',login_required(CommunitySongCreateView.as_view()),name='community_song_create'),
    url(r'^(?P<video_id>[-\w]+)/song/delete/(?P<pk>\d+)/(?P<slug>[-\w]+)/$',login_required(CommunitySongDeleteView.as_view()),name='community_song_delete'),

    url(r'^user/(?P<slug>[-\w]+)/$',profile, name='user_profile'),
    url(r'^edit_profile/$',login_required(UserProfileEditView.as_view()),name='edit_profile'),
    url(r'^deactivate_user/$',deactivate_user, name='deactivate_user'),
    url(r'^admin/', include(admin.site.urls)),
    url(r'^captcha/', include('captcha.urls')),
    url(r'^accounts/', include('allauth.urls')),
    url(r'^(?P<video_id>[-\w]+)/$',video,name='video_detail'),

    
]
