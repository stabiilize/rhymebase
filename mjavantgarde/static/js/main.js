(function(main, $, undefined){

	Data = {

	}
	API = {
		get_lne_items : function(){

		},
		vote : function(pk,vote_action,_type){

			var upvote = $('.upvote[data-pk="'+pk+'"]');
			var downvote = $('.downvote[data-pk="'+pk+'"]');
			var vote_count = $('.vote-count[data-pk="'+pk+'"]');
			if (vote_action == 'up'){
				if (upvote.hasClass('voted') && !downvote.hasClass('voted')){
					//upvote orange, downvote none
					upvote.removeClass('voted');
					vote_count.text(Number(vote_count.text())-1);
					vote_action = 'delete';
				}else if(!upvote.hasClass('voted') && !downvote.hasClass('voted')){
					//upvote none, downvote none
					upvote.addClass('voted');
					vote_count.text(Number(vote_count.text())+1);
				}else if(upvote.hasClass('voted') && downvote.hasClass('voted')){
					//upvote orange, downvote orange
					//this should never happen
				}else if(!upvote.hasClass('voted') && downvote.hasClass('voted')){
					//upvote none, downvote orange
					downvote.removeClass('voted');
					upvote.addClass('voted');
					vote_count.text(Number(vote_count.text())+2)
				}
			}else if (vote_action == 'down'){
				if (downvote.hasClass('voted') && !upvote.hasClass('voted')){
					//downvote orange, upvote none
					downvote.removeClass('voted');
					vote_count.text(Number(vote_count.text())+1);
					vote_action = 'delete'
				}else if(!downvote.hasClass('voted') && !upvote.hasClass('voted')){
					//downvote none, upvote none
					downvote.addClass('voted');
					vote_count.text(Number(vote_count.text())-1);
				}else if(upvote.hasClass('voted') && downvote.hasClass('voted')){
					//upvote orange, downvote orange
					//this should never happen
				}else if(!downvote.hasClass('voted') && upvote.hasClass('voted')){
					//downvote none, upvote orange
					upvote.removeClass('voted');
					downvote.addClass('voted');
					vote_count.text(Number(vote_count.text())-2)
				}
			}

			var data = {
				'pk' : pk,
				'vote_action' : vote_action,
				'_type': _type,
			};

			$.ajax({
                url:'/api/vote',
                type:'GET',
                datatype:'json',
                data:data,
                success: function(data){	
                	console.log(data);
                },
            });
		},
		get_user_voted : function(_type){
			var data = {
				_type : _type, 
				video_id : Data.video_id,
			}
			$.ajax({
                url:'/api/get_user_voted',
                type:'GET',
                datatype:'json',
                data:data,
                success: function(data){
                	data = JSON.parse(data.user_voted);
                	console.log(data);
                	$('.thevote').each(function(i,item){
                		$this = $(this);
                		if(data[i]=='voted_up'){
                			$this.find('.upvote').addClass('voted');
                		}else if(data[i]=='voted_down'){
                			$this.find('.downvote').addClass('voted');
                		}	
                	});
                },
            });
		},
		get_vote_data : function(rhyme_pk,vote_action){
			var data = {
				'rhyme_pk' : rhyme_pk,
				'vote_action' : vote_action
			};
			$.ajax({
                url:'/api/get_vote_data',
                type:'GET',
                datatype:'json',
                data:data,
                success: function(data){

                },
            });
		},
		get_community_rhymes : function(original_rhyme_pk){
			var data = {
				'rhyme_pk' : original_rhyme_pk	
			};
			$.ajax({
                url:'/api/get_community_rhymes',
                type:'GET',
                datatype:'json',
                data:data,
                success: function(data){

                    Data.community_rhymes = JSON.parse(data.community_rhymes);

                    var $modal_body = $('.modal-body');
                    var $container = $('<table/>',{
                        style: 'width:100%;height:auto;'
                    });
                    $modal_body.html('');
                    $modal_body.append($container)
                    $(Data.community_rhymes).each(function(i,item){
                        var $tr = $('<tr/>');
                        var $left_td = $('<td/>',{
                        	class: 'thevote'
                        });
                        var $middle_td = $('<td/>');
                        var $right_td = $('<td/>');
                        var $upvote_button = $('<button/>',{
                            style: 'float:left;',
                            'data-pk': item.pk,
                            'data-type': 'rhyme',
                            class: 'upvote votey'
                        });
                        var $downvote_button = $('<button/>',{
                            style: 'float:left;',
                            'data-pk': item.pk,
                            'data-type': 'rhyme',
                            class: 'downvote votey',
                        });

                        var $caret_up = $('<i/>',{
	                        	class: 'fa fa-caret-up',
	                        });
	                    var $caret_down = $('<i/>',{
	                       	class: 'fa fa-caret-down',
	                    });
                        var $vote_count = $('<div/>',{
	                        	text: item.fields.vote_score+' votes',
	                        	class: 'vote-count',
	                        	'data-pk': item.pk,
	                        	title: item.fields.num_vote_up+' up, '+item.fields.num_vote_down+' down',
	                        });	
                        var $text = $('<div/>',{
                            text: item.fields.start +' + '+item.fields.duration+': "'+item.fields.full_line+'"',
                            style: 'float:left;'
                        });
                        $upvote_button.append($caret_up);
                        $left_td.append($upvote_button);
                        $downvote_button.append($caret_down);
                        $left_td.append($downvote_button);
                        $middle_td.append($vote_count);
                        $right_td.append($text);
                        $tr.append($left_td);
                        $tr.append($middle_td);
                        $tr.append($right_td);
                        $container.append($tr);
                        Template.setup.click_events();
                    });
					API.get_user_voted('rhyme')
                },
            });
		},
		get_community_genres : function(video_id){
			var data = {
				'video_id' : video_id,
			};
			$.ajax({
                url:'/api/get_community_genres',
                type:'GET',
                datatype:'json',
                data:data,
                success: function(data){

                	Data.community_genres = JSON.parse(data.community_genres);

                    var $modal_body = $('.modal-body');
                    var $container = $('<table/>',{
                        style: 'width:100%;height:auto;'
                    });
                    $modal_body.html('');
                    if(Data.community_genres.length==0){
                    	$modal_body.append('No genres defined for this video. <a href="/'+video_id+'/genre/define">Define One</a>');
                    }else{
                    	$modal_body.append($container);
                    	$(Data.community_genres).each(function(i,item){
	                        var $tr = $('<tr/>');
	                        var $left_td = $('<td/>',{
	                        	class: 'thevote'
	                        });
	                        var $middle_td = $('<td/>');
	                        var $right_td = $('<td/>');
	                       
	                        var $upvote_button = $('<button/>',{
	                            style: 'float:left;',
	                            'data-pk': item.pk,
	                            'data-type':'genre',
	                            class: 'upvote votey'
	                        });
	                        var $downvote_button = $('<button/>',{
	                            style: 'float:left;',
	                            'data-pk': item.pk,
	                            'data-type':'genre',
	                            class: 'downvote votey',
	                        });

	                        var $caret_up = $('<i/>',{
	                        	class: 'fa fa-caret-up',
	                        });
	                        var $caret_down = $('<i/>',{
	                        	class: 'fa fa-caret-down',
	                        });

	                        var $vote_count = $('<div/>',{
	                        	text: item.fields.vote_score,
	                        	class: 'vote-count',
	                        	'data-pk': item.pk,
	                        	title: item.fields.num_vote_up+' up, '+item.fields.num_vote_down+' down',
	                        });	
	                        var $a0 = $('<a/>',{
	                        	href: '/genre/'+item.fields.genre_object,
	                            text: item.fields.genre_object+' ',
	                            style: 'float:left;'
	                        });
	                        var $a = $('<a/>',{
	                        	href: '/user/'+Data.community_genres[0].fields.user[0],
	                        	text: ' (submitted by: '+Data.community_genres[0].fields.user[0]+')'
	                        });
	                        $upvote_button.append($caret_up)
	                        $left_td.append($upvote_button);
	                        $downvote_button.append($caret_down);
	                        $left_td.append($downvote_button);
	                        $middle_td.append($vote_count);
	                        $right_td.append($a0);
	                        $right_td.append($a);
	                        $tr.append($left_td);
	                        $tr.append($middle_td);
	                        $tr.append($right_td);
	                        $container.append($tr);
	                        Template.setup.click_events();
	                    });
						API.get_user_voted('genre')
                    }
                },
            });
		},
		get_community_artists : function(video_id){
			var data = {
				'video_id' : video_id,
			};
			$.ajax({
                url:'/api/get_community_artists',
                type:'GET',
                datatype:'json',
                data:data,
                success: function(data){
                	Data.community_artists = JSON.parse(data.community_artists);

                    var $modal_body = $('.modal-body');
                    var $container = $('<table/>',{
                        style: 'width:100%;height:auto;'
                    });
                    $modal_body.html('');
                    if (Data.community_artists.length==0){
                    	$modal_body.append('No artists defined for this video. <a href="/'+video_id+'/artist/define">Define One</a>');
                    }else{
                    	$modal_body.append($container);
                    	$(Data.community_artists).each(function(i,item){
	                        var $tr = $('<tr/>');
	                        var $left_td = $('<td/>',{
	                        	class: 'thevote'
	                        });
	                        var $middle_td = $('<td/>');
	                        var $right_td = $('<td/>');
	                        var $upvote_button = $('<button/>',{
	                            style: 'float:left;',
	                            'data-pk': item.pk,
	                            'data-type': 'artist',
	                            class: 'upvote votey'
	                        });
	                        var $downvote_button = $('<button/>',{
	                            style: 'float:left;',
	                            'data-pk': item.pk,
	                            'data-type': 'artist',
	                            class: 'downvote votey',
	                        });

	                        var $caret_up = $('<i/>',{
	                        	class: 'fa fa-caret-up',
	                        });
		                    var $caret_down = $('<i/>',{
		                       	class: 'fa fa-caret-down',
		                    });

	                        var $vote_count = $('<div/>',{
	                        	text: item.fields.vote_score+' votes',
	                        	class: 'vote-count',
	                        	'data-pk': item.pk,
	                        	title: item.fields.num_vote_up+' up, '+item.fields.num_vote_down+' down',
	                        });
	                        var $text = $('<div/>',{
	                            text: item.fields.artist_object + ' (submitted by '+Data.community_artists[0].fields.user[0]+')',
	                            style: 'float:left;'
	                        });
	                        var $a = $('<a/>',{
	                        	href: '/user/'+Data.community_artists[0].fields.user[0],
	                        	text: '(submitted by: '+Data.community_artists[0].fields.user[0]+')'
	                        });
	                        $upvote_button.append($caret_up);
	                        $left_td.append($upvote_button);
	                        $downvote_button.append($caret_down);
	                        $left_td.append($downvote_button);
	                        $middle_td.append($vote_count);
	                        $text.append($a);
	                        $right_td.append($text);
	                        $tr.append($left_td);
	                        $tr.append($middle_td);
	                        $tr.append($right_td);
	                        $container.append($tr);
	                        Template.setup.click_events();
	                    });
						API.get_user_voted('artist')
                    }	
                    
                },
            });
		},
		get_community_songs : function(video_id){
			var data = {
				'video_id' : video_id,
			};
			$.ajax({
                url:'/api/get_community_songs',
                type:'GET',
                datatype:'json',
                data:data,
                success: function(data){
                	Data.community_songs = JSON.parse(data.community_songs);

                    var $modal_body = $('.modal-body');
                    var $container = $('<table/>',{
                        style: 'width:100%;height:auto;'
                    });
                    $modal_body.html('');
                    if (Data.community_songs.length == 0){
                    	$modal_body.append('No songs defined for this video. <a href="/'+video_id+'/song/define">Define One</a>');
                    }else{
                    	$modal_body.append($container);
	                    $(Data.community_songs).each(function(i,item){
	                        var $tr = $('<tr/>');
	                        var $left_td = $('<td/>',{
	                        	class: 'thevote'
	                        });
	                        var $middle_td = $('<td/>');
	                        var $right_td = $('<td/>');
	                        var $upvote_button = $('<button/>',{
	                            style: 'float:left;',
	                            'data-pk': item.pk,
	                            'data-type': 'song',
	                            class: 'upvote'
	                        });
	                        var $downvote_button = $('<button/>',{
	                            style: 'float:left;',
	                            'data-pk': item.pk,
	                            'data-type': 'song',
	                            class: 'downvote',
	                        });

	                        var $caret_up = $('<i/>',{
	                        	class: 'fa fa-caret-up',
	                        });
		                    var $caret_down = $('<i/>',{
		                       	class: 'fa fa-caret-down',
		                    });

	                        var $vote_count = $('<div/>',{
	                        	text: item.fields.vote_score+' votes',
	                        	class: 'vote-count',
	                        	'data-pk': item.pk,
	                        	title: item.fields.num_vote_up+' up, '+item.fields.num_vote_down+' down',
	                        });
	                        var $text = $('<div/>',{
	                            text: item.fields.song_object + ' (submitted by '+Data.community_songs[0].fields.user[0]+')',
	                            style: 'float:left;'
	                        });
	                        var $a = $('<a/>',{
	                        	href: '/user/'+Data.community_genres[0].fields.user[0],
	                        	text: '(submitted by: '+Data.community_genres[0].fields.user[0]+')'
	                        });
	                        $upvote_button.append($caret_up);
	                        $left_td.append($upvote_button);
	                        $downvote_button.append($caret_down);
	                        $left_td.append($downvote_button);
	                        $middle_td.append($vote_count);
	                        $text.append($a);
	                        $right_td.append($text);
	                        $tr.append($left_td);
	                        $tr.append($middle_td);
	                        $tr.append($right_td);
	                        $container.append($tr);
	                        Template.setup.click_events();
	                    });
						API.get_user_voted('song')
					}
                },
            });
		}
	}
	Template = {
		render : function(){
			Template.setup.load_poly_bg();
			Template.setup.click_events();
		},
		setup : {
			load_poly_bg : function(){
				canvases = [{
                    id: 'trcanvastop',
                    h: 600
                }];
                var w = $(window).width();
                patterns = {}
                $(canvases).each(function(i, item) {
                    patterns[item.id] = Trianglify({
                        height: item.h,
                        width: w,
                        //seed: 'bfcabecdxxzqvy',
                        //bfcabecdxxzqvy
                        cell_size: 50,
                        x_colors: ['#000024', '#8BC8E8', '#070718'],
           			 	y_colors: ['#CFD7F4', '#1889C4', '#cfd7f4'],
                        //seed: 'PRGn',
                    });
                    patterns[item.id].canvas(
                    document.getElementById(item.id));
                });
			},
			click_events : function(){

				$('.get-community-rhymes').click(function(){
					var $this = $(this);
					var rhyme_pk = $this.data('rhyme-pk');
		            API.get_community_rhymes(rhyme_pk);
		        });

		        $('.get-community-genres').click(function(){
					var $this = $(this);
					var video_id = $this.data('video-id');
		            API.get_community_genres(video_id);
		        });

		        $('.get-community-artists').click(function(){
					var $this = $(this);
					var video_id = $this.data('video-id');
		            API.get_community_artists(video_id);
		        });

		        $('.get-community-songs').click(function(){
					var $this = $(this);
					var video_id = $this.data('video-id');
		            API.get_community_songs(video_id);
		        });

		        $('.upvote').unbind("click").click(function(){
		        	var $this = $(this);
		        	var rhyme_pk = $this.data('pk');
		        	var _type = $this.data('type');
		        	API.vote(rhyme_pk,'up',_type);
		        	
		        });

		        $('.downvote').unbind("click").click(function(){
		        	console.log('downvote clicked');
		        	var $this = $(this);
		        	var rhyme_pk = $this.data('pk');
		        	var _type = $this.data('type');
		        	API.vote(rhyme_pk,'down',_type);
		        	
		        });
			},	
		}
	}
	Video = {
		
	}
	Utils = {
		queryString : function () {
      		var query_string = {};
      		var query = window.location.search.substring(1);
      		var vars = query.split("&");
      		for (var i=0;i<vars.length;i++) {
        		var pair = vars[i].split("=");
        		if (typeof query_string[pair[0]] === "undefined") {
          			query_string[pair[0]] = decodeURIComponent(pair[1]);
            	} else if (typeof query_string[pair[0]] === "string") {
          		var arr = [ query_string[pair[0]],decodeURIComponent(pair[1]) ];
          		query_string[pair[0]] = arr;
        		} else {
          			query_string[pair[0]].push(decodeURIComponent(pair[1]));
        		}
      		} 
      		return query_string;
    	},
	}
	$(document).ready(function($){
		Template.render();
	});

}(window.main = window.main || {}, jQuery));