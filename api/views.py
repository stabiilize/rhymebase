import json
import jsonpickle
import random
import operator

from django.shortcuts import render
from django.contrib.auth.decorators import login_required
from django.views.decorators.csrf import csrf_protect
from django.contrib.auth.models import User
from django.core.serializers import serialize,deserialize
from django.http import JsonResponse
from django.forms import model_to_dict
	
from engine.models import Video
from engine.models import Rhyme
from engine.models import AssortedRhymes
from engine.models import CommunityGenre
from engine.models import CommunityArtist
from engine.models import CommunitySong 

from lib.utilities import *

# lne = likely needs editing

@json_response
def get_lne_items(request):
	context = {}
	if request.is_ajax():
		if request.method == 'GET':
			lne_items = Rhyme.objects.filter(likely_needs_editing=True)[:50]
			context['lne_items'] = lne_items
			return context

def vote(request):
	context = {}
	if request.is_ajax():
		if request.method == 'GET':
			args = request.GET
			vote_action = args['vote_action']
			pk = args['pk']
			_type = args['_type']
			types = {
				'rhyme': Rhyme,
				'genre': CommunityGenre,
				'artist': CommunityArtist,
				'song': CommunitySong,
			}
			object_type = types[_type]
			_object = object_type.objects.get(pk=pk)
			if vote_action == 'up':
				_object.votes.up(request.user.id)
				print 'voted up'
			elif vote_action == 'down':
				_object.votes.down(request.user.id)
				print 'voted down'
			elif vote_action == 'delete':
				_object.votes.delete(request.user.id)
				print 'deleted vote'
			context['message'] = '%s voted:%s %s' % (request.user,_object,vote_action)
			return JsonResponse(context)

@csrf_protect
@login_required
@json_response
def get_vote_data(request):
	context = {}
	if request.is_ajax():
		if request.method == 'GET':
			args = request.GET
			rhyme_pk = args['rhyme_pk']
			vote_action = args['vote_action']
			if vote_action == 'exists':
				context['message'] = rhyme.votes.exists(request.user.id)
			elif vote_action == 'all':
				context['message'] = rhyme.votes.all(request.user.id)
			elif vote_action == 'user_ids':
				context['message'] = rhyme.votes.user_ids(request.user.id)
			elif vote_action == 'count':
				context['message'] = rhyme.votes.count(request.user.id)
			return context

def get_top_contributors(request):
	context = {}
	if request.is_ajax():
		if request.method == 'GET':
			args = request.GET
			rr = Rhyme.objects.all()[0]
			users = User.objects.all()
			user_votes = {u.username:u.userprofile.get_karma() for u in users}	
			sorted_uv = sorted(
				user_votes.items(), 
				key=operator.itemgetter(1), 
				reverse=True)[:10]
			sorted_uv_and_statuses = zip(
				sorted_uv,
				[User.objects.get(username=su[0]).userprofile.status for su in sorted_uv]
			)
			context['top_contributors'] = sorted_uv_and_statuses
			return JsonResponse(context)

def get_community_rhymes(request):
	context = {}
	if request.is_ajax():
		if request.method == 'GET':
			args = request.GET
			rhyme_pk = int(args['rhyme_pk'])
			rhyme = Rhyme.objects.get(id=rhyme_pk)
			if rhyme.original_rhyme:
				community_rhymes = list(rhyme.original_rhyme.community_rhymes.all())
				community_rhymes.append(rhyme.original_rhyme)
			else:
				community_rhymes = list(rhyme.community_rhymes.all())
				community_rhymes.append(rhyme)
			community_rhymes = serialize(
				'json', 
				community_rhymes, 
				use_natural_foreign_keys=True, 
				use_natural_primary_keys=True)
			context['community_rhymes'] = community_rhymes
			return JsonResponse(context)

def get_user_voted(request):
	context = {}
	if request.is_ajax():
		if request.method == 'GET':
			args = request.GET
			video_id = args['video_id']
			_type = args['_type']
			video = Video.objects.get(video_id=video_id)
			types = {
				'rhyme': video.rhymes.all(),
				'genre': video.community_genres.all(),
				'artist': video.community_artists.all(),
				'song': video.community_songs.all(),
			}
			objects = types[_type]
			user_voted = [o.get_user_voted(request.user.id) for o in objects]
			context['user_voted'] = json.dumps(user_voted)
			return JsonResponse(context)

def get_community_genres(request):
	context = {}
	if request.is_ajax():
		if request.method == 'GET':
			args = request.GET
			video_id = args['video_id']
			video = Video.objects.get(video_id=video_id)
			community_genres = video.community_genres.all()
			context['community_genres'] = serialize(
				'json', 
				community_genres, 
				use_natural_foreign_keys=True, 
				use_natural_primary_keys=True)
			return JsonResponse(context)

def get_community_artists(request):
	context = {}
	if request.is_ajax():
		if request.method == 'GET':
			args = request.GET
			video_id = args['video_id']
			video = Video.objects.get(video_id=video_id)
			community_artists = video.community_artists.all()
			community_artists = serialize(
				'json', 
				community_artists, 
				use_natural_foreign_keys=True, 
				use_natural_primary_keys=True)
			context['community_artists'] = community_artists
			return JsonResponse(context)

def get_community_songs(request):
	context = {}
	if request.is_ajax():
		if request.method == 'GET':
			args = request.GET
			video_id = args['video_id']
			video = Video.objects.get(video_id=video_id)
			community_songs = video.community_songs.all()
			community_songs = serialize(
				'json', 
				community_songs, 
				use_natural_foreign_keys=True, 
				use_natural_primary_keys=True)
			context['community_songs'] = community_songs
			return JsonResponse(context)
