from django.conf.urls import include, url
from django.contrib.auth.decorators import login_required
from django.contrib import admin
from . import views

urlpatterns = [
	url(r'^vote/$',views.vote,name="vote"),
	url(r'^get_vote_data/$',views.get_vote_data, name="get_vote_data"),
	url(r'^get_lne_items/$',views.get_lne_items, name="get_lne_items"),
	url(r'^get_community_genres/$',views.get_community_genres, name="get_community_genres"),
	url(r'^get_community_artists/$',views.get_community_artists, name="get_community_artists"),
	url(r'^get_community_songs/$',views.get_community_songs, name="get_community_songs"),
	url(r'^get_community_rhymes/$',views.get_community_rhymes, name="get_community_rhymes"),
	url(r'^get_top_contributors/$',views.get_top_contributors, name="get_top_contributors"),
	url(r'^get_user_voted/$',views.get_user_voted, name="get_user_voted"),

]